VERSION=`git describe --tags`
BUILD_TIME=`date +%FT%T%z`
LDFLAGS=-ldflags "-X main.Version=${V} -X main.BuildTime=${BUILD_TIME}"
NAME=device

gen-code:
	protoc --go_out=. --go-grpc_out=. grpc/proto/*.proto

build-docker-img:
	docker build -t $(NAME):dev .
	docker rmi -f $$(docker images --filter “dangling=true” -q --no-trunc)

run: build
	./bin/$(NAME)

build: clear
	go build ${LDFLAGS} -o ./bin/$(NAME) ./main.go
	./bin/$(NAME) -v

switch-context:
	kubectl config use-context ml-lko2

forward: switch-context
	kubectl port-forward svc/device-core-api-svc 19083:9083 --address 0.0.0.0

clear:
	rm -rf ./bin/$(SER)

clear-untag-image:
	docker rmi -f $$(docker images --filter “dangling=true” -q --no-trunc)


# ## Push image

# gcloud auth configure-docker
# docker tag <image-name>:<tag> asia.gcr.io/muulin-universal/<image-name>:<tag>
# docker push asia.gcr.io/muulin-universal/<image-name>:<tag>

# ## get Kubernetes config
# gcloud container clusters get-credentials muulin-gcp-1 --zone=asia-east1

# ## test 
# kubectl get nodes