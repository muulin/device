# Auth Service


This project is Device Service designed to mananger device.

## Prerequirement

- MongoDB

## 開發環境安裝


1. [install golang](https://go.dev/doc/install)

	golang 版本建議為：`1.18` 以上

2. [install Docker](https://docs.docker.com/engine/install/)

3. [develop infrastructure deploy]()
```bash
# 啟動mongo container
git clone
cd mongo
sh start.sh

cd redis
docker-compose up -d
```

### How to get source code

```bash
git clone git@bitbucket.org:muulin/auth.git
```

### Develop config file sample
```yaml
mongo:
    uri: mongodb://mongo1:27017,mongo2:27018,mongo3:27019/?replicaSet=myReplicaSet&ssl=false&readPreference=primary
    defaul: dev-ml-v5-sys

log:
    level: debug
    target: os

redis:
    host: redis:6379
    pass: ""
    dbMap:
      device: 1

kafka:
    brokers: ["localhost:9092"]
    
```



## Environment Variable

```env
# assign config path
environment=dev|beta|prod

# assign api service port
API_PORT=9080

# assign grpc service port
GRPC_PORT=7080

# allow Cross-Origin Resource Sharing        
API_CORS=true|false 

# select api mode
GIN_MODE=debug|test|release

# select api permission mode
API_AUTH_MODE=release|mock

# name for service as log prefix
SERVICE=auth

# set config file root path
CONF_PATH=./config/

# secret code for admin to create user
Auth_Code=42697846

# trusted proxies
TRUSTED_PROXIES=127.0.0.1

# kafka topic to remote
KAFKA_TOPIC_REMOTE=mqtt.remote

# kafka topic to set time
KAFKA_TOPIC_SET_TIME=mqtt.set-time

# kafka topic to update channel rawdata
KAFKA_TOPIC_RAWDATA=channel.rawdata

# kafka topic to update channel realtime data
KAFKA_TOPIC_REALTIME=channel.realtime

# kafka topic to channel backup data
KAFKA_TOPIC_BACKUP=channel.backup

# log level
LOG_LEVEL=debug|info|warn|error|fatal

# log message send to target
LOG_TARGET=os|fluent
```


### Run project

```bash
make run
```
or
```bash
go run .
```

## 資料夾說明

- api - api handler
- grpc - grpc service & protobuf
- dao - data asscess object
- input - user input model
- model - functional logic
- doc - documentation


## 專案技術

- [gin web framework](https://github.com/gin-gonic/gin)
- [grpcurl](https://github.com/fullstorydev/grpcurl)




## CI/CD 說明

> 待撰寫

此專案有使用 Github Actions，所以發起 PR 時會自動執行以下動作：

- 建立 Node.js 環境
- 安裝相依套件
- 編譯程式碼
- 執行 ESLint 掃描
- 執行測試
...

當專案 merge 到 main 時會自動執行以下動作：

- 建立 Node.js 環境
- 安裝相依套件
- 編譯程式碼
- 執行 ESLint 掃描
- 執行測試
- 部署到 Github Pages
...

