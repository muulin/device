package api

import (
	"errors"

	"github.com/94peter/sterna"
	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/kafka"
	"github.com/gin-gonic/gin"
)

func getCache(c *gin.Context) (db.RedisClient, error) {
	di := getServiceDI(c)
	if di == nil {
		return nil, errors.New("service di not exist")
	}
	servName := di.GetServiceName()
	clt, err := di.NewRedisClientDB(c, di.GetDB(servName))
	if err != nil {
		return nil, err
	}
	return clt, nil
}

type serviceDI interface {
	db.RedisDI
	kafka.ConfigDI
	sterna.CommonDI
}

func getServiceDI(c *gin.Context) serviceDI {
	val, ok := c.Get(string(sterna.CtxServDiKey))
	if !ok {
		return nil
	}
	return val.(serviceDI)
}
