package api

import (
	"net/http"

	"device/input"
	"device/model/device/v1"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/94peter/sterna/api"
	apiErr "github.com/94peter/sterna/api/err"
	"github.com/94peter/sterna/auth"
	"github.com/94peter/sterna/dao"
	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/log"
	"github.com/gin-gonic/gin"
)

func NewDeviceApi(s string) api.GinAPI {
	return &deviceAPI{
		ErrorOutputAPI: api.NewErrorOutputAPI(s),
	}
}

type deviceAPI struct {
	api.ErrorOutputAPI
}

func (a *deviceAPI) GetAPIs() []*api.GinApiHandler {
	return []*api.GinApiHandler{
		{Path: "/v1/device", Method: "POST", Handler: a.createHandler, Auth: true},
		{Path: "/v1/device", Method: "GET", Handler: a.getHandler, Auth: true},
		{Path: "/v1/device/:id", Method: "GET", Handler: a.getDetailHandler, Auth: true},
		{Path: "/v1/device/:id/notification/offline", Method: "PUT", Handler: a.offlineNotifySetHandler, Auth: true},
		{Path: "/lua/device/:id/channel", Method: "GET", Handler: a.getChannelHandler, Auth: true},
	}
}

func (a *deviceAPI) GetName() string {
	return "device"
}

func (a *deviceAPI) createHandler(c *gin.Context) {
	in := &input.CreateDeviceV1{}
	err := c.Bind(in)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}

	c.String(http.StatusOK, "Hello POST")
}

func (a *deviceAPI) getHandler(c *gin.Context) {
	clt, err := getCache(c)
	if err != nil {
		a.GinOutputErr(c, err)
		return
	}
	dbclt := db.GetMgoDBClientByGin(c)
	l := log.GetLogByGin(c)
	realtimeInfo := device.NewRealtimeInfo(c, dbclt.GetCoreDB(), clt, l)
	rd, err := realtimeInfo.AllAvaliableDevice()
	if err != nil {
		a.GinOutputErr(c, err)
		return
	}
	result, count := dao.Format(rd, func(i interface{}) map[string]interface{} {
		if obj, ok := i.(*device.RealtimeDevice); ok {
			return map[string]interface{}{
				"id":           obj.ID.Hex(),
				"description":  obj.Description,
				"macAddress":   obj.Device.MACaddress,
				"gwid":         obj.Device.GwID,
				"virtualId":    obj.Device.VirtualID,
				"timezone":     obj.Device.Timezone,
				"channel":      obj.Device.Channel,
				"notification": obj.Device.Notification,
				"data":         obj.RawdataCache,
			}
		}
		return nil
	})
	if count == 0 {
		c.Writer.WriteHeader(http.StatusNoContent)
		return
	}
	c.JSON(http.StatusOK, result)
}

func (a *deviceAPI) getDetailHandler(c *gin.Context) {
	id := c.Param("id")
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}
	clt, err := getCache(c)
	if err != nil {
		a.GinOutputErr(c, err)
		return
	}
	dbclt := db.GetMgoDBClientByGin(c)
	l := log.GetLogByGin(c)
	realtimeInfo := device.NewRealtimeInfo(c, dbclt.GetCoreDB(), clt, l)
	rd, err := realtimeInfo.FindByID(oid)
	if err != nil {
		a.GinOutputErr(c, err)
		return
	}
	if rd == nil || rd.Device == nil {
		c.Writer.WriteHeader(http.StatusNoContent)
		return
	}
	c.Writer.Header().Add("X-Channel", rd.Device.Channel)
	c.JSON(http.StatusOK, map[string]any{
		"id":           rd.ID.Hex(),
		"description":  rd.Description,
		"macAddress":   rd.Device.MACaddress,
		"gwid":         rd.Device.GwID,
		"virtualId":    rd.Device.VirtualID,
		"timezone":     rd.Device.Timezone,
		"channel":      rd.Device.Channel,
		"notification": rd.Device.Notification,
	})
}

func (a *deviceAPI) offlineNotifySetHandler(c *gin.Context) {
	id := c.Param("id")
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}
	var in input.UpdateOfflineNotification
	err = c.Bind(&in)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}
	dbclt := db.GetMgoDBClientByGin(c)
	l := log.GetLogByGin(c)
	crud := device.NewCRUD(c, dbclt.GetCoreDB(), l)
	user := auth.GetUserByGin(c)
	err = crud.UpdateOfflineNotification(oid, &in, user)
	if err != nil {
		a.GinOutputErr(c, err)
		return
	}
	c.Writer.WriteString("ok")
}

func (a *deviceAPI) getChannelHandler(c *gin.Context) {
	id := c.Param("id")
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}

	dbclt := db.GetMgoDBClientByGin(c)
	l := log.GetLogByGin(c)
	crud := device.NewCRUD(c, dbclt.GetCoreDB(), l)
	rd, err := crud.FindById(oid)
	if err != nil {
		a.GinOutputErr(c, err)
		return
	}
	if rd == nil || rd.Device == nil {
		c.Writer.WriteHeader(http.StatusNoContent)
		return
	}
	c.Writer.Header().Add("X-Channel", rd.Device.Channel)
}
