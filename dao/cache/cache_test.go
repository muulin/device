package cache_test

import (
	"reflect"
	"testing"
	"time"

	"device/dao/cache"
)

func TestEncodeMap(t *testing.T) {
	// Test case 1: Testing with nil object
	obj := (*cache.RawdataCache)(nil)
	result, err := obj.EncodeMap()
	if err == nil {
		t.Errorf("Expected error for nil object, but got nil")
	}
	if len(result) != 0 {
		t.Errorf("Expected empty map, but got %v", result)
	}
	testTime := time.Date(2000, time.October, 1, 0, 0, 0, 0, time.UTC)
	// Test case 2: Testing with a non-nil object
	obj = &cache.RawdataCache{
		Key:          "value1",
		LatestTime:   testTime,
		DataInterval: time.Minute,
		OnlineState:  "green",
		RemoteCmd:    "cmd",
	}

	expected := map[string]string{
		"Key":          "value1",
		"LatestTime":   testTime.Format(time.RFC3339),
		"DataInterval": time.Minute.String(),
		"OnlineState":  "green",
		"ErrorMessage": "",
		"RemoteCmd":    "cmd",
	}
	result, err = obj.EncodeMap()
	if err != nil {
		t.Errorf("Expected nil error, but got %v", err)
	}

	if !reflect.DeepEqual(result, expected) {
		t.Errorf("Expected %v, but got %v", expected, result)
	}
}

func TestDecodeMap(t *testing.T) {
	obj := &cache.RawdataCache{}
	testTime := time.Date(2000, time.October, 1, 0, 0, 0, 0, time.UTC)
	m := map[string]string{
		"Key":          "value1",
		"LatestTime":   testTime.Format(time.RFC3339),
		"DataInterval": time.Minute.String(),
		"OnlineState":  "green",
		"ErrorMessage": "",
		"RemoteCmd":    "cmd",
	}

	err := obj.DecodeMap(m)
	if err != nil {
		t.Errorf("Error decoding map: %v", err)
	}

	// Check if the fields were set correctly
	if obj.Key != "value1" {
		t.Errorf("Expected Key to be 'value1', got '%s'", obj.Key)
	}

	if !obj.LatestTime.Equal(testTime) {
		t.Errorf("Expected LatestTime to be 2, got %s", obj.LatestTime.Format(time.RFC3339))
	}

	if obj.DataInterval != time.Minute {
		t.Errorf("Expected DataInterval to be '1m0s', got '%s'", obj.DataInterval.String())
	}

}
