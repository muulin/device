package cache

import (
	"bytes"
	"encoding/base64"
	"encoding/gob"
	"fmt"
	"reflect"
	"strconv"
	"time"

	mydao "device/dao"

	"github.com/94peter/sterna/dao"
	"github.com/go-redis/redis/v8"
)

type RawdataCache struct {
	Key          string
	RemoteCmd    string
	LatestTime   time.Time
	OnlineState  string
	DataInterval time.Duration
	ErrorMessage string
	common       dao.ComCacheObj
}

func (t *RawdataCache) GetKey() string {
	return t.Key
}

func (t *RawdataCache) Encode() ([]byte, error) {
	return t.common.Encode(t)
}

func (t *RawdataCache) Decode(data []byte) error {
	return t.common.Decode(data, t)
}

func (t *RawdataCache) DecodePipe() error {
	return t.common.DecodePipe(t)
}

func (s *RawdataCache) SetStringCmd(sc *redis.StringCmd) {
	s.common.SetStringCmd(sc)
}

func (s *RawdataCache) GetStringCmd() (*redis.StringCmd, error) {
	return s.common.GetStringCmd()
}

func (s *RawdataCache) GetError() error {
	return s.common.GetError()
}

func (s *RawdataCache) HasError() bool {
	return s.common.HasError()
}

func (obj *RawdataCache) EncodeMap() (map[string]string, error) {
	if obj == nil {
		return nil, fmt.Errorf("data is nil")
	}
	result := map[string]string{}
	// reflect point RawdataCache to map[string]string
	t := reflect.TypeOf(*obj)

	// Iterate over the fields of the struct
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)

		// Get the field value
		value := reflect.ValueOf(obj).Elem().Field(i)

		// Convert the field value to a string
		fieldValue := ""
		switch value.Kind() {
		case reflect.String:
			fieldValue = value.String()
		case reflect.Int, reflect.Int64:
			if value.Type() == reflect.TypeOf(time.Duration(0)) {
				fieldValue = value.Interface().(time.Duration).String()
			} else {
				fieldValue = strconv.FormatInt(value.Int(), 10)
			}
		case reflect.Bool:
			fieldValue = fmt.Sprintf("%t", value.Bool())
		case reflect.Struct:
			if value.Type() == reflect.TypeOf(time.Time{}) {
				fieldValue = value.Interface().(time.Time).Format(time.RFC3339)
			}
		default:
			// Handle other types as needed
			continue
		}

		// Add the field name and value to the map
		result[field.Name] = fieldValue
	}

	return result, nil
}
func (obj *RawdataCache) DecodeMap(m map[string]string) error {

	if obj == nil {
		return fmt.Errorf("data is nil")
	}

	// Get the type of RawdataCache
	t := reflect.TypeOf(*obj)

	// Iterate over the fields of the struct
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)

		// Get the field value from the map
		fieldValue, ok := m[field.Name]
		if !ok {
			continue
		}

		// Set the field value in the RawdataCache object
		value := reflect.ValueOf(obj).Elem().FieldByName(field.Name)
		if !value.IsValid() || !value.CanSet() {
			continue
		}

		switch value.Kind() {
		case reflect.String:
			value.SetString(fieldValue)
		case reflect.Int, reflect.Int64:
			if value.Type() == reflect.TypeOf(time.Duration(0)) {
				durationVal, err := time.ParseDuration(fieldValue)
				if err != nil {
					return err
				}
				value.Set(reflect.ValueOf(durationVal))
			} else {
				intValue, err := strconv.ParseInt(fieldValue, 10, 64)
				if err != nil {
					return err
				}
				value.SetInt(intValue)
			}
		case reflect.Bool:
			boolValue, err := strconv.ParseBool(fieldValue)
			if err != nil {
				return err
			}
			value.SetBool(boolValue)
		case reflect.Struct:
			if value.Type() == reflect.TypeOf(time.Time{}) {
				timeValue, err := time.Parse(time.RFC3339, fieldValue)
				if err != nil {
					return err
				}
				value.Set(reflect.ValueOf(timeValue))
			}
		default:
			// Handle other types as needed
			continue
		}
	}

	return nil

}

type ValueCache interface {
	dao.CacheMapObj
	IsEmpty() bool
	GetValue(k uint32) *mydao.Sensor
	GetValues() map[uint32]*mydao.Sensor
	SetValue(uint32, float64, uint8, time.Time)
	CopyWithoutTimeOutSensor(t time.Time, interval time.Duration) map[uint32]*mydao.Sensor
}

func NewSetValuesCache(deviceID string) ValueCache {
	return &valuesCache{
		DeviceID: deviceID,
		Type:     "set",
		Values:   make(map[uint32]*mydao.Sensor),
	}
}

func NewGetValuesCache(deviceID string) ValueCache {
	return &valuesCache{
		DeviceID: deviceID,
		Type:     "get",
		Values:   make(map[uint32]*mydao.Sensor),
	}
}

type valuesCache struct {
	DeviceID string
	Type     string
	Values   map[uint32]*mydao.Sensor
}

func (s *valuesCache) IsEmpty() bool {
	return len(s.Values) == 0
}

func (s *valuesCache) GetValue(k uint32) *mydao.Sensor {
	return s.Values[k]
}

func (s *valuesCache) CopyWithoutTimeOutSensor(t time.Time, interval time.Duration) map[uint32]*mydao.Sensor {
	data := make(map[uint32]*mydao.Sensor)
	interval = interval + time.Minute
	for k, v := range s.Values {
		if t.Sub(v.UploadTime) > interval {
			continue
		}
		data[k] = v
	}
	return data
}
func (t *valuesCache) GetValues() map[uint32]*mydao.Sensor {
	return t.Values
}

func (t *valuesCache) SetValue(k uint32, v float64, dp uint8, uploadTime time.Time) {
	t.Values[k] = &mydao.Sensor{
		Dp:         dp,
		Value:      v,
		UploadTime: uploadTime,
	}
}
func (t *valuesCache) GetKey() string {
	return fmt.Sprintf("%s@%s", t.DeviceID, t.Type)
}

func (t *valuesCache) EncodeMap() (map[string]string, error) {
	// Values to map
	result := map[string]string{}
	for k, v := range t.Values {
		// encode v to gob
		var buffer bytes.Buffer
		err := gob.NewEncoder(&buffer).Encode(v)
		if err != nil {
			return nil, err
		}
		// encode v to base64 string
		result[strconv.FormatUint(uint64(k), 10)] = base64.StdEncoding.EncodeToString(buffer.Bytes())
	}
	return result, nil
}
func (t *valuesCache) DecodeMap(m map[string]string) error {
	if t.Values == nil {
		t.Values = make(map[uint32]*mydao.Sensor)
	}
	for k, v := range m {
		decodeByte, err := base64.StdEncoding.DecodeString(v)
		if err != nil {
			return err
		}
		var sensor *mydao.Sensor
		err = gob.NewDecoder(bytes.NewReader(decodeByte)).Decode(&sensor)
		if err != nil {
			return err
		}
		v, err := strconv.ParseUint(k, 10, 32)
		if err != nil {
			return err
		}
		t.Values[uint32(v)] = sensor
	}
	return nil
}
