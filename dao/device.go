package dao

import (
	"device/types"

	"github.com/94peter/sterna/util"
)

type Device struct {
	MACaddress  string
	VirtualID   uint8
	GwID        string
	Timezone    int32
	Model       types.DeviceModel
	State       types.DeviceState
	Channel     string
	Description string
}

func (d *Device) GetMqttCmdTopic() string {
	return "cmd/" + util.MD5(d.MACaddress+d.GwID)
}
