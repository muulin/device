package dao

import (
	"errors"

	"bitbucket.org/muulin/interlib/device/pb"
)

type PublishDeviceData struct {
	MacAddress  string             `json:"mac"`
	VirtualId   int                `json:"virtualId"`
	Type        pb.DataType        `json:"type"`
	Values      []*deviceDataValue `json:"values"`
	LatestTime  uint32             `json:"lastestTime"`
	OnlineState string             `json:"onlineState"`
	Now         uint32             `json:"nowTime"`
}

func (dd *PublishDeviceData) AddValue(key uint32, val float64) {
	dd.Values = append(dd.Values, &deviceDataValue{
		Key:   key,
		Value: val,
	})
}

type deviceDataValue struct {
	Key   uint32  `json:"key"`
	Value float64 `json:"value"`
}

type PublishSetTime struct {
	Topic    string `json:"topic"`
	Timezone int32  `json:"timezone"`
}

type PublishRemote struct {
	Topic string `json:"topic"`
	Cmds  []*cmd `json:"cmds"`
}

func (pr *PublishRemote) AddCmd(key string, val float64) error {
	for _, c := range pr.Cmds {
		if c.Key == key {
			return errors.New("duplacate key: " + key)
		}
	}
	pr.Cmds = append(pr.Cmds, &cmd{Key: key, Value: val})
	return nil
}

type cmd struct {
	Key   string  `json:"key"`
	Value float64 `json:"value"`
}
