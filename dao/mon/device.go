package mon

import (
	"errors"
	"fmt"
	"regexp"
	"strings"

	deviceDao "device/dao"

	"github.com/94peter/sterna/dao"
	"github.com/94peter/sterna/util"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func NewDeviceID(mac string, vID uint8) (oID primitive.ObjectID, returnErr error) {
	if !util.IsMAC(mac) {
		returnErr = errors.New("not a valid MAC")
		return
	}
	virtualID := fmt.Sprintf("%.4x", vID)
	reg, err := regexp.Compile("[^a-fA-F0-9]+")
	if err != nil {
		returnErr = errors.New("regexp compile failed: " + err.Error())
		return
	}

	newMac := reg.ReplaceAllString(mac, "")
	if len(newMac) != 12 {
		returnErr = errors.New("not 12 char MAC address")
		return
	}
	newMac = strings.ToLower(newMac)

	newMac += "00000000"
	newMac += virtualID

	oID, err = primitive.ObjectIDFromHex(newMac)
	if err != nil {
		returnErr = errors.New("ObjectIDFromHex() error: " + err.Error())
		return
	}
	return

}

const (
	deviceC = "device"
)

type Device struct {
	ID                primitive.ObjectID `bson:"_id"`
	*deviceDao.Device `bson:",inline"`
	deviceDao.Notification
}

func (d *Device) GetID() interface{} {
	return d.ID
}

func (d *Device) GetC() string {
	return deviceC
}

func (d *Device) GetDoc() interface{} {
	return d
}

func (d *Device) GetIndexes() []mongo.IndexModel {
	return []mongo.IndexModel{
		{
			Keys: bson.D{
				{Key: "state", Value: 1},
				{Key: "macaddress", Value: 1},
				{Key: "model", Value: 1},
			},
		},
		{
			Keys: bson.D{
				{Key: "macaddress", Value: 1},
				{Key: "model", Value: 1},
			},
		},
		{
			Keys: bson.D{
				{Key: "macaddress", Value: 1},
				{Key: "gwid", Value: 1},
			},
			Options: options.Index().SetUnique(true).SetPartialFilterExpression(
				bson.D{
					{Key: "gwid", Value: bson.M{
						"$gt": "",
					}},
				},
			),
		},
	}
}

func (d *Device) AddRecord(u dao.LogUser, msg string) []*dao.Record {
	return nil
}

func (d *Device) SetCreator(u dao.LogUser) {
}
