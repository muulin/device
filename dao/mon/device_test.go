package mon_test

import (
	"testing"

	"device/dao/mon"

	"github.com/stretchr/testify/assert"
)

func Test_NewDeviceID(t *testing.T) {
	monDevice, err := mon.NewDeviceID("3F:56:89:AB:CD:EF", 6)
	assert.Nil(t, err)

	assert.Equal(t, "3f5689abcdef0000000000ff", monDevice.Hex())
}
