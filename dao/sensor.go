package dao

import "time"

type Sensor struct {
	Value      float64
	Dp         uint8
	UploadTime time.Time
}
