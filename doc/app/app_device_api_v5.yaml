openapi: 3.0.0
info:
  title: App-device-API
  version: "1.0.0"
  license:
    name: Apache 2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
    
paths:
  /v5/device:
    get:
      tags: [v5]
      summary: 列出所有設備 (admin show all ; editor show partial)
      operationId: get-device-list-v5
      security:
        - bearerAuth: ["ophead", "op"]
      parameters:
        - in: query
          name: keyword
          required: false
          schema:
            type: string
          description: 關鍵字 (e.g. Mac、Model)
        - in: query
          name: state
          required: false
          schema:
            type: string
            enum: ["assigned", "used", "2bRepaired", "recycle"]
          description: 狀態
        - in: query
          name: projectID
          required: false
          schema:
            type: string
          description: 案場id
        - in: query
          name: page
          required: false
          schema:
            type: integer
          description: 目前頁數
        - in: query
          name: limit
          required: false
          schema:
            type: integer
          description: 一頁幾筆      
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  rows:
                    type: array
                    items:
                      $ref: '#/components/schemas/DeviceListItem'
                  allPages:
                    type: integer
                    description: 總頁數
                  page:
                    type: integer
                    description: 目前頁數
                  limit:
                    type: integer
                    description: 一頁幾筆
                  total:
                    type: integer
        "204":
          description: 沒有產品
        '400':
          description: bad request (parameters error)
        '401':
          description: unauthorized
        '500':
          description: internal server error
  /v5/device/{ID}:
    get:
      tags: [v5]
      summary: 產品的詳細資料 (admin, editor)
      operationId: get-device-detail-v5
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: ID
          required: true
          schema:
            type: string    
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DeviceDetail'
        '400':
          description: bad request (parameters error)
        '401':
          description: unauthorized
        '404':
          description: ID not found
        '500':
          description: internal server error 
    put:
      tags: [v5]
      summary: 更改產品的資訊 (admin)
      operationId: put-device-v5
      security:
        - bearerAuth: ["ophead"]
      parameters:
        - in: path
          name: ID
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                description:
                  type: string
                accessUsers:
                  type: array
                  items:
                    type: object
                    properties:
                      account:
                        type: string
                      name:
                        type: string
      responses:
        '200':
          description: OK
          content:
            text/plain:
              schema:
                type: string
                example: "ok"
        '400':
          description: bad request
        '401':
          description: unauthorized
        '404':
          description: ID not found
        '500':
          description: internal server error
  /v5/device/txn/sending:
    post:
      tags: [new-api]
      summary: 建立設備配送單 (admin, editor)
      operationId: create-device-txn-sending
      security:
        - bearerAuth: []
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/PostDeviceTxnSending'
      responses:
        '200':
          description: OK
        '400':
          description: bad request
        '401':
          description: unauthorized
        '500':
          description: internal server error
    get:
      tags: [new-api]
      summary: 取得設備配送單 (admin, editor)
      operationId: get-device-txn-sending-v5
      description: startTime&endTime的間距最多6個月
      security:
        - bearerAuth: []
      parameters:
        - in: query
          name: state
          schema:
            type: string
            enum: ["new", "confirmed"]
          required: true
        - in: query
          name: startTime
          schema:
            type: string
          required: false
        - in: query
          name: endTime
          schema:
            type: string
          required: false
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ListDeviceTxnSending'
        '400':
          description: bad request (parameters error)
        '401':
          description: unauthorized
        '404':
          description: ID not found
        '500':
          description: internal server error
  /v5/device/txn/sending/{ID}/migration:
    post:
      tags: [new-api]
      summary: 搬移設備配送單 (admin)
      operationId: migrate-device-txn-sending
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: ID
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/MigrateDeviceTxnSending'
      responses:
        '200':
          description: OK
        '400':
          description: bad request
        '401':
          description: unauthorized
        '500':
          description: internal server error
  /v5/device/txn/recycling:
    post:
      tags: [new-api]
      summary: 建立回收單 (admin, editor)
      operationId: create-device-txn-recycling-v5
      security:
        - bearerAuth: []
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateRecyclingTxn'
      responses:
        '200':
          description: OK
          
        '400':
          description: bad request (parameters error)
        '401':
          description: unauthorized
        '404':
          description: ID not found
        '500':
          description: internal server error
    get:
      tags: [new-api]
      summary: 取得設備回收單 (admin, editor)
      operationId: get-device-txn-recycling-v5
      description: startTime&endTime的間距最多6個月
      security:
        - bearerAuth: []
      parameters:
        - in: query
          name: state
          schema:
            type: string
            enum: ["new", "confirmed"]
          required: true
        - in: query
          name: startTime
          schema:
            type: string
          required: false
        - in: query
          name: endTime
          schema:
            type: string
          required: false
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ListDeviceTxnRecycling'
        '400':
          description: bad request (parameters error)
        '401':
          description: unauthorized
        '404':
          description: ID not found
        '500':
          description: internal server error
  /v5/device/txn/{ID}:
    put:
      tags: [new-api]
      summary: 更改設備回收單 (admin all, editor partial)
      operationId: modify-device-txn-recycling
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: ID
          required: true
          schema:
            type: string
        - in: header
          name: X-ServiceType
          required: true
          schema:
            type: string
            enum: ["admin", "operator"]
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ModifyDeviceTxnRecycling'
      responses:
        '200':
          description: OK
        '400':
          description: bad request
        '401':
          description: unauthorized
        '500':
          description: internal server error
    delete:
      tags: [new-api]
      summary: 刪除設備回收單 (admin)
      operationId: delete-device-txn-recycling
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: ID
          required: true
          schema:
            type: string
        - in: header
          name: X-ServiceType
          required: true
          schema:
            type: string
            enum: ["admin", "operator"]
      responses:
        '200':
          description: OK
          content:
            text/plain:
              schema:
                type: string
                example: "ok"
        '401':
          description: unauthorized
        '404':
          description: ID not found
        '500':
          description: internal server error
  /v5/device/txn/{ID}/confirmation:
    put:
      tags: [new-api]
      summary: 確認設備配送單 (admin, editor)
      operationId: confirm-device-txn-sending
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: ID
          required: true
          schema:
            type: string
        - in: header
          name: X-ServiceType
          required: true
          schema:
            type: string
            enum: ["admin", "operator"]
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ConfirmDeviceTxnSending'
      responses:
        '200':
          description: OK
        '400':
          description: bad request
        '401':
          description: unauthorized
        '500':
          description: internal server error
  /v5/device/txn/{ID}/cancellation:
    put:
      tags: [new-api]
      summary: 取消配送單 (admin, editor)
      operationId: cancel-device-txn-sending
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: ID
          required: true
          schema:
            type: string 
        - in: header
          name: X-ServiceType
          required: true
          schema:
            type: string
            enum: ["admin", "operator"]
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CancelDeviceTxn'
      responses:
        '200':
          description: OK
        '400':
          description: bad request
        '401':
          description: unauthorized
        '500':
          description: internal server error
  /v5/device/txn/{ID}/comment:
    post:
      tags: [new-api]
      summary: 建立配送單或回收單回覆訊息 (admin, editor)
      operationId: create-device-txn-comment
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: ID
          required: true
          schema:
            type: string
          description: TxnID
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateComment'
      responses:
        '200':
          description: OK
        "204":
          description: 沒有產品
        '400':
          description: bad request (parameters error)
        '401':
          description: unauthorized
        '500':
          description: internal server error
  /v5/device/{ID}/replacing:
    get:
      tags: [new-api]
      summary: 裝置替換對應設備及案場 (admin all, editor partial)
      operationId: get-replace-device
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: ID
          required: true
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/GetReplaceDevice'
        '400':
          description: bad request (parameters error)
        '401':
          description: unauthorized
        '404':
          description: ID not found
        '500':
          description: internal server error 
    post:
      tags: [new-api]
      summary: 裝置替換 (admin all, editor partial)
      operationId: replace-device
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: ID
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ReplaceDevice'
      responses:
        '200':
          description: OK
          
        '400':
          description: bad request (parameters error)
        '401':
          description: unauthorized
        '404':
          description: ID not found
        '500':
          description: internal server error 
components:
  securitySchemes:
    bearerAuth:            # arbitrary name for the security scheme
      type: http
      scheme: bearer
      bearerFormat: JWT
  schemas:
    comments:
      type: array
      items:
        type: object
        properties:
          time:
            type: string
          account:
            type: string
          name:
            type: string
          msg:
            type: string
    txnDevice:
      type: object
      properties:
        id:
          type: string
        mac:
          type: string
        virtualID:
          type: string
        model:
          type: string
        msg:
          type: string
    deviceActionLog:
      type: object
      properties:
        time:
          type: string
          description: 日期 (2016-07-14T00:00:00Z)
          example: "2016-07-14T00:00:00Z"
        state:
          type: string
          enum: ["assigned", "2bRepaired", "recycle"]
        remark:
          type: string
          example: 備注
    CreateRecyclingTxn:
      type: object
      properties:
        devices:
          type: array
          items:
            type: object
            properties:
              id:
                type: string
              msg:
                type: string
        description:
          type: string
          example: 備注
    ConfirmDeviceTxnSending:
      type: object
      properties:
        description:
          type: string
    DeviceListItem:
      type: object
      properties:
        id:
          type: string
          example: "objectID"
        macAddress:
          type: string
        state:
          type: string
          enum: ["assigned", "2bRepaired", "recycle"]
        description:
          type: string
        model:
          type: string
          enum: ["IoT627", "miniGW", "scada"]
        onlineState:
            type: string
            enum: ["green", "yellow", "orange", "red"]
            description: 連線狀態
    DeviceDetail:
      type: object
      properties:
        id:
          type: string
          example: "objectID"
        macAddress:
          type: string
        state:
          type: string
          enum: [ "assigned", "2bRepaired", "recycle"]
        description:
          type: string
        model:
          type: string
          enum: ["IoT627", "miniGW", "scada"]
        onlineState:
            type: string
            enum: ["green", "yellow", "orange", "red"]
            description: 連線狀態
        last20log:
          type: array
          items:
            $ref: '#/components/schemas/deviceActionLog'
    ModifyDeviceTxnRecycling:
      type: object
      properties:
        deviceActions:
          type: array
          items:
            type: object
            properties:
              act:
                type: string
                enum: ["add", "remove"]
              id:
                type: string
              msg:
                type: string
        comment:
          type: string
      example:
        deviceActions:
          - act: add
            id: 611b47a2421aa9000a669f94
            msg: "開不了機"
          - act: remove
            id: 611b47a2421aa9000a669f94
        comment: 選錯設備更換
    ListDeviceTxnSending:
      type: array
      items:
        type: object
        properties:
          id:
            type: string
          time:
            type: string
          devices:
            type: array
            items:
              $ref: '#/components/schemas/txnDevice'
          description:
            type: string
          comments:
            $ref: '#/components/schemas/comments'
    GetReplaceDevice:
      type: array
      items:
        type: object
        properties:
          project:
            type: string
          equipment:
            type: array
            items:
              type: object
              properties:
                name:
                  type: string
    ReplaceDevice:
      type: object
      properties:
        deviceID:
          type: string
    CreateComment:
      type: object
      properties:
        msg:
          type: string
    ListDeviceTxnRecycling:
      type: array
      items:
        type: object
        properties:
          id:
            type: string
          time:
            type: string
          devices:
            type: array
            items:
              $ref: '#/components/schemas/txnDevice'
          description:
            type: string
          comments:
            $ref: '#/components/schemas/comments'
    CancelDeviceTxn:
      type: object
      properties:
        comment:
          type: string
    PostDeviceTxnSending:
      type: object
      properties:
        deviceIDs:
          type: array
          items:
            type: string
        description:
          type: string
    MigrateDeviceTxnSending:
      type: object
      properties:
        channel:
          type: string
servers:
  # Added by API Auto Mocking Plugin
  - description: SwaggerHub API Auto Mocking
    url: https://virtserver.swaggerhub.com/PeterGroup/channel-device/1.0.0