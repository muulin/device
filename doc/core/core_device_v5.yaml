openapi: 3.0.0
info:
  title: Core-device-API
  version: "1.0.0"
  license:
    name: Apache 2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
    
paths:
  /v5/device:
    post:
      tags: [device]
      summary: 建立設備 (admin)
      operationId: create-device
      security:
        - bearerAuth: []
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/PostDevice'
      responses:
        '200':
          description: OK
        '400':
          description: bad request
        '401':
          description: unauthorized
        '500':
          description: internal server error
    get:
      tags: [device]
      summary: 列出所有/關鍵字案場 (admin)
      operationId: get-device-list-v1
      security:
        - bearerAuth: []
      parameters:
        - in: query
          name: keyword
          required: false
          schema:
            type: string
          description: 關鍵字 (e.g. Mac、Model)
        - in: query
          name: state
          required: false
          schema:
            type: string
          description: 狀態
        - in: query
          name: channel
          required: false
          schema:
            type: string
          description: 頻道
        - in: query
          name: page
          required: false
          schema:
            type: integer
          description: 目前頁數
        - in: query
          name: limit
          required: false
          schema:
            type: integer
          description: 一頁幾筆      
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  rows:
                    type: array
                    items:
                      $ref: '#/components/schemas/DeviceListItem'
                  allPages:
                    type: integer
                    description: 總頁數
                  page:
                    type: integer
                    description: 目前頁數
                  limit:
                    type: integer
                    description: 一頁幾筆
                  total:
                    type: integer
        "204":
          description: 沒有產品
        '400':
          description: bad request (parameters error)
        '401':
          description: unauthorized
        '500':
          description: internal server error
  /v5/device/scada:
    post:
      tags: [device]
      summary: 建立SCADA設備 (admin)
      operationId: create-scada-device
      security:
        - bearerAuth: []
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/PostScadaDevice'
      responses:
        '200':
          description: OK
        '400':
          description: bad request
        '401':
          description: unauthorized
        '500':
          description: internal server error
  /v5/device/_export:
    post:
      tags: [device]
      summary: 用csv上傳產品入庫資料 (admin)
      operationId: post-device-v1
      security:
        - bearerAuth: []
      parameters: 
        - in: query
          name: encode
          required: true
          schema:
            type: string
            enum: ["utf8", "big5"]
      requestBody:
        description: columns (model, mac_address, description)
        content:
          application/octet-stream: 
            schema:
              type: string
              format: binary
      responses:
        '200':
          description: OK
          content:
            text/plain:
              schema:
                type: string
                example: "ok"
        '400':
          description: bad request (request body error)
          content:
            application/json:
              schema:
                type: object
                properties:
                  failedData:
                    type: array
                    items:
                      type: object
                      properties:
                        description:
                          type: string
                          example: "im description"
                        mac_address: 
                          type: string
                          example: "00:00:00:00:00:00"
                        model:
                          type: string
                          example: "miniGW"
                  totalFailed:
                    type: integer
                    example: 1
                    description: 總共失敗多少筆
        '401':
          description: unauthorized
        '500':
          description: internal server error
  /v5/device/{ID}:
    get:
      tags: [device]
      summary: 產品的詳細資料 (admin)
      operationId: get-device-detail-v1
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: ID
          required: true
          schema:
            type: string    
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DeviceDetail'
        '400':
          description: bad request (parameters error)
        '401':
          description: unauthorized
        '404':
          description: ID not found
        '500':
          description: internal server error 
    put:
      tags: [device]
      summary: 更改產品的資訊 (admin)
      operationId: put-device-v1
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: ID
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                model: 
                  type: string
                description:
                  type: string
      responses:
        '200':
          description: OK
          content:
            text/plain:
              schema:
                type: string
                example: "ok"
        '400':
          description: bad request
        '401':
          description: unauthorized
        '404':
          description: ID not found
        '500':
          description: internal server error 
    delete:
      tags: [device]
      summary: 刪除產品 (admin)
      operationId: delete-device-v1
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: ID
          required: true
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            text/plain:
              schema:
                type: string
                example: "ok"
        '401':
          description: unauthorized
        '404':
          description: ID not found
        '500':
          description: internal server error
  /v5/device/{ID}/_set:
    post:
      tags: [device]
      summary: 遠端設值 (admin)
      operationId: set-device-v5
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: ID
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                device: 
                  type: integer
                address:
                  type: integer
                value:
                  type: number
                dp:
                  type: integer
      responses:
        '200':
          description: OK
          content:
            text/plain:
              schema:
                type: string
                example: "ok"
        '400':
          description: bad request
        '401':
          description: unauthorized
        '404':
          description: ID not found
        '500':
          description: internal server error
  /v5/device/{ID}/data:
    get:
      tags: [device]
      summary: 取得controller/sensor的最新value (admin)
      operationId: get-device-latest-v1
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: ID
          required: true
          schema:
            type: string
          description: deviceID
        - in: query
          name: type
          required: true
          schema:
            type: string
            enum: ['both', 'controller', 'sensor']
          description: 類型 
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  controller:
                    $ref: '#/components/schemas/DeviceLatestValue'
                  sensor:
                    $ref: '#/components/schemas/DeviceLatestValue'
        "204":
          description: 沒有產品
        '400':
          description: bad request (parameters error)
        '401':
          description: unauthorized
        '500':
          description: internal server error
  /v5/device/{ID}/repairing:
    post:
      tags: [new-api]
      summary: 建立裝置維修單 (admin)
      operationId: create-device-repairing
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: ID
          required: true
          schema:
            type: string
          description: DeviceID
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateDeviceRepairing'
      responses:
        '200':
          description: OK
        "204":
          description: 沒有產品
        '400':
          description: bad request (parameters error)
        '401':
          description: unauthorized
        '500':
          description: internal server error
  /v5/device/repairing:
    get:
      tags: [new-api]
      summary: 取得通路裝置回收單 (admin)
      operationId: get-device-repairing
      security:
        - bearerAuth: []
      description: startTime&endTime的間距最多6個月
      parameters:
        - in: query
          name: state
          schema:
            type: string
            enum: ["new", "reparable", "irreparable"]
          required: true
        - in: query
          name: startTime
          schema:
            type: string
          required: false
        - in: query
          name: endTime
          schema:
            type: string
          required: false
        - in: query
          name: limit
          schema:
            type: integer
          required: false
        - in: query
          name: page
          schema:
            type: integer
          required: false
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/GetDeviceRepairingPaginator'
        "204":
          description: 沒有產品
        '400':
          description: bad request (parameters error)
        '401':
          description: unauthorized
        '500':
          description: internal server error
  /v5/device/repairing/{ID}:
    put:
      tags: [new-api]
      summary: 回報裝置維修結果 (admin)
      operationId: report-device-repairing
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: ID
          required: true
          schema:
            type: string
          description: RepairingID
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ReportDeviceRepairing'
      responses:
        '200':
          description: OK
        "204":
          description: 沒有產品
        '400':
          description: bad request (parameters error)
        '401':
          description: unauthorized
        '500':
          description: internal server error
  /v5/device/repairing/{ID}/comment:
    post:
      tags: [new-api]
      summary: 建立維修單回覆訊息 (admin)
      operationId: create-device-repairing-comment
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: ID
          required: true
          schema:
            type: string
          description: RepairingID
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateComment'
      responses:
        '200':
          description: OK
        "204":
          description: 沒有產品
        '400':
          description: bad request (parameters error)
        '401':
          description: unauthorized
        '500':
          description: internal server error
components:
  securitySchemes:
    bearerAuth:            # arbitrary name for the security scheme
      type: http
      scheme: bearer
      bearerFormat: JWT
  schemas:
    PostDevice:
      type: array
      items:
        type: object
        properties:
          mac:
            type: string
            example: "55:33:33:33:33:11"
          model:
            type: string
            enum: ["IoT627", "miniGW"] 
            example: "IoT627"
          description:
            type: string
            example: "special colour"
    PostScadaDevice:
      type: array
      items:
        type: object
        properties:
          mac:
            type: string
            example: "55:33:33:33:33:11"
          virtualID:
            type: integer
            description: (0~255)
          description:
            type: string
            example: "special colour"
    ReportDeviceRepairing:
      type: object
      properties:
        result:
          type: string
          enum: ["reparable", "irreparable"]
        comment:
          type: string
    CreateDeviceRepairing:
      type: object
      properties:
        description:
          type: string
    DeviceLatestValue:
      type: object
      properties:
        lastUpdatedTime:
          type: string
          example: "2016-07-14T00:00:00Z"
        data:
          type: object
          additionalProperties:
            type: object
            properties:
              value:
                type: number
              time:
                type: string
              dp:
                type: integer
          example:
            257:
              value: 22.5
              time: "2022-07-28T00:00:00Z"
              dp: 1
            258:
              value: 22.5
              time: "2022-07-28T00:00:00Z"
              dp: 1
    DeviceListItem:
      type: object
      properties:
        id:
          type: string
          example: "objectID"
        macAddress:
          type: string
        state:
          type: string
          enum: ["stock", "assigned", "2bRepaired", "recycle" ,"repairing", "discard"]
        channel:
          type: string
          example: "HK"
        description:
          type: string
        model:
          type: string
          enum: ["IoT627", "miniGW", "scada"]
        onlineState:
            type: string
            enum: ["green", "yellow", "orange", "red"]
            description: 連線狀態
    DeviceDetail:
      type: object
      properties:
        id:
          type: string
          example: "objectID"
        macAddress:
          type: string
        state:
          type: string
          enum: ["stock", "assigned", "2bRepaired", "repairing", "discard"]
        channel:
          type: string
          example: "HK"
        description:
          type: string
        model:
          type: string
          enum: ["IoT627", "miniGW", "scada"]
        onlineState:
            type: string
            enum: ["green", "yellow", "orange", "red"]
            description: 連線狀態
        last20log:
          type: array
          items:
            $ref: '#/components/schemas/deviceActionLog'
    deviceActionLog:
      type: object
      properties:
        time:
          type: string
          description: 日期 (2016-07-14T00:00:00Z)
          example: "2016-07-14T00:00:00Z"
        state:
          type: string
          enum: ["stock", "assigned", "2bRepaired", "repairing", "discard"]
        remark:
          type: string
          example: 備注
        channel:
          type: string
          example: HK
    GetDeviceRepairingPaginator:
      type: object
      properties:
        rows:
          type: array
          items:
            type: object
            properties:
              id:
                type: string
                description: repairingID
              time:
                type: string
              deviceID:
                type: string
              mac:
                type: string
              description:
                type: string
              comments:
                $ref: '#/components/schemas/comments'
        allPages:
          type: integer
          description: 總頁數
        page:
          type: integer
          description: 目前頁數
        limit:
          type: integer
          description: 一頁幾筆
        total:
          type: integer
    comments:
      type: array
      items:
        type: object
        properties:
          time:
            type: string
          account:
            type: string
          name:
            type: string
          msg:
            type: string
    CreateComment:
      type: object
      properties:
        msg:
          type: string
