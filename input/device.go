package input

type CreateDeviceV1 struct {
	Box []*deviceV1
}

type deviceV1 struct {
	Name       string
	MacAddress string `json:"macAddress"`
	GwID       string `json:"boxID"`
	Timezone   string
}
