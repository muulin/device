package input

type UpdateOfflineNotification struct {
	Enabled bool `json:"enable"`
}
