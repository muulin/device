package input

import (
	"bitbucket.org/muulin/interlib/device/pb"
)

type UpsertRawdata struct {
	Type    pb.DataType
	Rawdata *pb.Rawdata
}

func (r *UpsertRawdata) Validate() error {
	return nil
}
