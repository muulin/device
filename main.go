package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"strings"
	"time"

	coreV1 "device/api"
	"device/model/device/v1"
	"device/mygrpc"
	"device/startup"

	"bitbucket.org/muulin/interlib/device/pb"

	coreInterceptor "bitbucket.org/muulin/interlib/core/interceptor"

	authMid "bitbucket.org/muulin/interlib/auth/mid"
	"github.com/94peter/sterna"
	"github.com/94peter/sterna/api"
	sternaMid "github.com/94peter/sterna/api/mid"
	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/kafka"
	sternaLog "github.com/94peter/sterna/log"
	"github.com/94peter/sterna/mystorage"
	"github.com/94peter/sterna/util"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/joho/godotenv"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	envMode   = flag.String("em", "local", "local, container")
	v         = flag.Bool("v", false, "version")
	Version   = "1.0.0"
	BuildTime = time.Now().Local().GoString()
)

func main() {
	flag.Parse()

	if *v {
		fmt.Println("Version: " + Version)
		fmt.Println("Build Time: " + BuildTime)
		return
	}

	if *envMode == "local" {
		err := godotenv.Load(".env")
		if err != nil {
			fmt.Println("No .env file")
		}
	}

	confPath := os.Getenv("CONF_PATH")
	if *envMode == "local" {
		confPath = util.StrAppend(confPath, os.Getenv("environment"), "/")
	}

	di := &di{}
	hdstore := mystorage.NewHdStorage(confPath)
	confByte, err := hdstore.Get("config.yml")
	if err != nil {
		panic(err)
	}
	sterna.InitConfByByte(confByte, di)
	if err = di.IsConfEmpty(); err != nil {
		panic("config setting error: " + err.Error())
	}

	go runStartUp(di)

	go func() {
		for {
			gRPC(di)
			time.Sleep(time.Second)
		}
	}()
	runApi(di)

}

func runStartUp(mydi *di) {
	time.Sleep(time.Minute * 2)
	ctx := context.Background()
	dbclt, err := mydi.NewMongoDBClient(ctx, "")
	if err != nil {
		panic(err)
	}
	defer dbclt.Close()
	redisclt, err := mydi.NewRedisClientDB(ctx, mydi.GetDB(mydi.GetServiceName()))
	if err != nil {
		panic(err)
	}
	defer redisclt.Close()
	l := mydi.NewLogger("startup")
	realtimeinfo := device.NewRealtimeInfo(ctx, dbclt.GetCoreDB(), redisclt, l)
	remote := device.NewKafkaRemote(ctx, mydi, l)
	startupServ := startup.New(realtimeinfo, remote)
	err = startupServ.Run()
	if err != nil {
		panic(err)
	}
}

func gRPC(mydi *di) {

	port := ":" + os.Getenv("GRPC_PORT")

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	authInter := coreInterceptor.NewAuthInterceptor()
	dbCoreInter := coreInterceptor.NewLocalDBInterceptor(mydi)

	s := grpc.NewServer(
		grpc.StreamInterceptor(
			grpc_middleware.ChainStreamServer(
				authInter.StreamServerInterceptor(),
				dbCoreInter.StreamServerInterceptor(),
			)),
		grpc.UnaryInterceptor(
			grpc_middleware.ChainUnaryServer(
				authInter.UnaryServerInterceptor(),
				dbCoreInter.UnaryServerInterceptor(),
			)),
	)
	reflection.Register(s)

	pb.RegisterDeviceServiceServer(s, mygrpc.NewDeviceService())
	pb.RegisterDeviceV1ServiceServer(s, mygrpc.NewServiceV1())
	pb.RegisterDeviceCronServiceServer(s, mygrpc.NewCronService())
	pb.RegisterDeviceV2ServiceServer(s, mygrpc.NewServiceV2())
	log.Printf("core gRPC server is running [%s].", port)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func runApi(mydi *di) {
	servName := mydi.GetServiceName()

	port := os.Getenv("API_PORT")
	ginMode := os.Getenv(("GIN_MODE"))
	authMode := os.Getenv(("API_AUTH_MODE"))
	logLevel := os.Getenv("LOG_LEVEL")
	authAddress := os.Getenv("GRPC_Auth_Address")
	if authAddress == "" {
		panic("missing auth address")
	}

	trustedProxiesEnv := os.Getenv("TRUSTED_PROXIES")
	if trustedProxiesEnv == "" {
		trustedProxiesEnv = "127.0.0.1"
	}

	prxies := strings.Split(trustedProxiesEnv, ",")

	var err error
	var authMiddle sternaMid.AuthGinMidInter
	if authMode == "mock" {
		authMiddle = sternaMid.NewMockAuthMid()
	} else {
		authMiddle, err = authMid.NewGinInterAuthMid(authAddress, servName)
		if err != nil {
			panic("invalid grpc auth service: " + authAddress)
		}
	}

	mids := []sternaMid.GinMiddle{
		authMiddle,
		sternaMid.NewGinFixDiMid(mydi, servName),
		sternaMid.NewGinDBMid(servName),
	}
	if logLevel == "debug" {
		mids = append([]sternaMid.GinMiddle{sternaMid.NewGinDebugMid(servName)}, mids...)
	}

	log.Printf("run api at port: {%s}, auth mode: {%s}", port, authMode)

	log.Fatal(api.NewGinApiServer(ginMode).
		SetAuth(authMiddle).
		Middles(mids...).
		AddAPIs(
			api.NewHealthAPI(servName),
			coreV1.NewDeviceApi(servName),
		).SetTrustedProxies(prxies).Run(port).Error())
}

type di struct {
	*db.MongoConf        `yaml:"mongo,omitempty"`
	*db.RedisConf        `yaml:"redis"`
	*kafka.KafkaConfig   `yaml:"kafka"`
	sternaLog.LoggerConf `yaml:"log"`
}

func (d *di) IsConfEmpty() error {
	if d.MongoConf == nil {
		return errors.New("mongo no set")
	}
	if os.Getenv("LOG_TARGET") == "fluent" &&
		d.LoggerConf.FluentLog == nil {
		return errors.New("log.FluentLog no set")
	}
	if d.RedisConf == nil {
		return errors.New("redis no set")
	}
	if d.KafkaConfig == nil {
		return errors.New("kafka no set")
	}
	return nil
}

func (d *di) GetServiceName() string {
	return os.Getenv("SERVICE")
}
