package device

import (
	cacheDao "device/dao/cache"
	"device/dao/mon"
	"time"

	"bitbucket.org/muulin/interlib/device/pb"

	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/log"
	"github.com/94peter/sterna/model/cache"
)

type Cache interface {
	GetStateMap([]*pb.Device) map[string]state
}

type cacheImpl struct {
	cache.Cache
	l log.Logger
}

func NewCache(clt db.RedisClient, l log.Logger) Cache {
	return &cacheImpl{
		Cache: cache.NewRedisCache(clt),
		l:     l,
	}
}

func (impl *cacheImpl) GetStateMap(devices []*pb.Device) map[string]state {
	keys := make([]string, len(devices))
	for i, d := range devices {
		id, err := mon.NewDeviceID(d.Mac, uint8(d.VirtualID))
		if err != nil {
			continue
		}
		keys[i] = id.Hex()
	}
	var rawdatas *cacheDao.RawdataCache
	result := make(map[string]state)
	objs, err := impl.Cache.GetObjs(keys, rawdatas)
	if err != nil {
		impl.l.Warn("get rawdata fail: " + err.Error())
	}
	for _, obj := range objs {
		if o, ok := obj.(*cacheDao.RawdataCache); ok && o.Key != "" {
			result[o.Key] = state{
				OnineState:   o.OnlineState,
				DataInterval: o.DataInterval,
			}
		}
	}
	return result
}

type state struct {
	OnineState   string
	DataInterval time.Duration
}
