package device

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"device/dao"
	"device/dao/mon"
	"device/input"
	"device/types"

	"bitbucket.org/muulin/interlib/device/pb"

	"github.com/94peter/sterna/auth"
	"github.com/94peter/sterna/log"
	"github.com/94peter/sterna/model/mgom"
	"github.com/94peter/sterna/util"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type CRUD interface {
	FindById(id primitive.ObjectID) (*mon.Device, error)
	FindByIds(ids []primitive.ObjectID) ([]*mon.Device, error)
	Create(*pb.CreateDeviceV1Request) (*mon.Device, error)
	GetVirtualId(*pb.GetVirtualIdRequest) (uint8, error)
	DeviceExist(*pb.CheckExistRequest) (*pb.CheckExistResponse, error)
	Remove(*pb.RemoveDeviceV1Request) error
	UpdateOfflineNotification(primitive.ObjectID, *input.UpdateOfflineNotification, auth.ReqUser) error
}

func NewCRUD(ctx context.Context, db *mongo.Database, l log.Logger) CRUD {
	return &crudImpl{
		dbmodel: mgom.NewMgoModel(ctx, db, l),
		log:     l,
	}
}

type crudImpl struct {
	dbmodel mgom.MgoDBModel
	log     log.Logger
}

func (crud *crudImpl) FindById(id primitive.ObjectID) (*mon.Device, error) {
	device := &mon.Device{
		ID: id,
	}
	err := crud.dbmodel.FindByID(device)
	if err != nil {
		return nil, err
	}
	return device, nil
}

func (crud *crudImpl) FindByIds(ids []primitive.ObjectID) ([]*mon.Device, error) {
	result, err := crud.dbmodel.Find(&mon.Device{}, bson.M{
		"_id": bson.M{
			"$in": ids,
		},
	})
	if err != nil {
		return nil, err
	}
	return result.([]*mon.Device), nil
}

func (crud *crudImpl) Create(req *pb.CreateDeviceV1Request) (*mon.Device, error) {
	vid, err := crud.genVirtualID(req.Mac)
	if err != nil {
		return nil, err
	}
	oid, err := mon.NewDeviceID(req.Mac, vid)
	if err != nil {
		return nil, err
	}
	device := &mon.Device{
		ID: oid,
		Device: &dao.Device{
			Channel:     req.Channel,
			MACaddress:  req.Mac,
			VirtualID:   vid,
			Timezone:    req.Timezone,
			Model:       types.Other,
			GwID:        req.Gwid,
			State:       types.Used,
			Description: req.Name,
		},
	}
	_, err = crud.dbmodel.Save(device, nil)
	if err != nil {
		return nil, err
	}
	return device, nil
}

func (crud *crudImpl) genVirtualID(mac string) (uint8, error) {
	device := &mon.Device{}
	list, err := crud.dbmodel.Find(device, primitive.M{
		"macaddress": mac,
	})
	if err != nil {
		return 0, err
	}
	max := -1
	devices := list.([]*mon.Device)
	for _, d := range devices {
		if v := int(d.VirtualID); v > max {
			max = v
		}
	}
	v := max + 1
	if v > 255 {
		return 0, errors.New("invalid virtual id")
	}
	return uint8(v), nil
}

func (crud *crudImpl) DeviceExist(req *pb.CheckExistRequest) (*pb.CheckExistResponse, error) {
	result := map[string]bool{}
	var deviceQ []primitive.M
	for _, d := range req.Devices {
		result[util.StrAppend(d.Mac, " - ", d.Gwid)] = false
		deviceQ = append(deviceQ, primitive.M{"macaddress": d.Mac, "gwid": d.Gwid})
	}
	device := &mon.Device{}
	list, err := crud.dbmodel.Find(device, primitive.M{
		"$or": deviceQ,
	})
	if err != nil {
		return nil, err
	}

	for _, d := range list.([]*mon.Device) {
		result[util.StrAppend(d.MACaddress, " - ", d.GwID)] = true
	}

	return &pb.CheckExistResponse{
		ExistMap: result,
	}, nil
}

func (crud *crudImpl) Remove(req *pb.RemoveDeviceV1Request) error {
	device := &mon.Device{}
	oid, err := mon.NewDeviceID(req.Mac, uint8(req.VirtualID))
	if err != nil {
		return err
	}
	device.ID = oid
	err = crud.dbmodel.FindByID(device)
	if err != nil {
		return err
	}
	if device.Channel != req.Channel {
		return fmt.Errorf("device channel [%s] not match request channel [%s]", device.Channel, req.Channel)
	}
	_, err = crud.dbmodel.RemoveByID(device, nil)
	return err
}

func (crud *crudImpl) GetVirtualId(req *pb.GetVirtualIdRequest) (uint8, error) {
	device := &mon.Device{}
	err := crud.dbmodel.FindOne(device, primitive.M{
		"macaddress": req.Mac,
		"gwid":       req.GwID,
	})
	if err != nil {
		if strings.Contains(err.Error(), "no documents in result") {
			return 0, nil
		}
		return 0, err
	}
	return device.VirtualID, nil
}

func (crud *crudImpl) UpdateOfflineNotification(id primitive.ObjectID, req *input.UpdateOfflineNotification, u auth.ReqUser) error {
	device := &mon.Device{
		ID: id,
	}
	err := crud.dbmodel.FindByID(device)
	if err != nil {
		return err
	}
	device.Notification.Offline = req.Enabled
	_, err = crud.dbmodel.UpdateOne(device, bson.D{
		{Key: "notification", Value: device.Notification},
	}, u)
	return err
}
