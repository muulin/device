package device

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"os"
	"time"

	"device/dao/cache"
	kafkaDao "device/dao/kafka"

	"bitbucket.org/muulin/interlib/device/pb"

	"github.com/94peter/sterna/kafka"
	"github.com/94peter/sterna/log"
)

type KafkaDeviceDataSend interface {
	Send(*RealtimeDevice, cache.ValueCache) error
	Close()
}

func NewKafkaDeviceDataSend(ctx context.Context, di kafka.ConfigDI) KafkaDeviceDataSend {
	return &kafkaDeviceDataImpl{
		CommonKafka: kafka.NewCommonKafka(ctx, di, os.Getenv("KAFKA_TOPIC_RAWDATA")),
		nowTime:     time.Now(),
	}
}

type kafkaDeviceDataImpl struct {
	kafka.CommonKafka
	nowTime time.Time
}

func (s *kafkaDeviceDataImpl) Send(real *RealtimeDevice, getValues cache.ValueCache) error {
	w := s.GetKafkaWriter()
	message, err := s.getMessage(real, getValues, s.nowTime)
	if err != nil {
		return err
	}
	err = w.Message(map[string][]byte{
		"X-Channel": []byte(real.Channel),
	}, message)
	if err != nil {
		return err
	}
	return nil
}

func (s *kafkaDeviceDataImpl) getMessage(real *RealtimeDevice, getValues cache.ValueCache, now time.Time) ([]byte, error) {
	var data *kafkaDao.PublishDeviceData
	if real.RawdataCache != nil {
		data = &kafkaDao.PublishDeviceData{
			MacAddress:  real.MACaddress,
			VirtualId:   int(real.VirtualID),
			Type:        pb.DataType_Sensor,
			LatestTime:  uint32(real.LatestTime.Unix()),
			OnlineState: real.OnlineState,
			Now:         uint32(now.Unix()),
		}
		sensors := getValues.CopyWithoutTimeOutSensor(now, real.DataInterval*time.Second)
		for k, v := range sensors {
			data.AddValue(k, v.Value)
		}
	} else {
		data = &kafkaDao.PublishDeviceData{
			MacAddress:  real.MACaddress,
			VirtualId:   int(real.VirtualID),
			Type:        pb.DataType_Sensor,
			OnlineState: "red",
			Now:         uint32(now.Unix()),
		}
	}
	return json.Marshal(data)
}

type KafkaSetTimeSend interface {
	Send(*RealtimeDevice) error
	Close()
}

func NewKafkaSetTime(ctx context.Context, di kafka.ConfigDI) KafkaSetTimeSend {
	return &kafkaSetTimeImpl{
		CommonKafka: kafka.NewCommonKafka(ctx, di, os.Getenv("KAFKA_TOPIC_SET_TIME")),
	}
}

type kafkaSetTimeImpl struct {
	kafka.CommonKafka
}

func (s *kafkaSetTimeImpl) Send(device *RealtimeDevice) error {
	w := s.GetKafkaWriter()
	message, err := s.getMessage(device)
	if err != nil {
		return err
	}
	err = w.Message(nil, message)
	if err != nil {
		return err
	}
	return nil
}

func (s *kafkaSetTimeImpl) getMessage(d *RealtimeDevice) ([]byte, error) {
	var data *kafkaDao.PublishSetTime

	if d.RawdataCache != nil && d.RemoteCmd != "" {
		data = &kafkaDao.PublishSetTime{
			Topic:    d.RawdataCache.RemoteCmd,
			Timezone: d.Timezone,
		}
	} else {
		data = &kafkaDao.PublishSetTime{
			Topic:    d.Device.GetMqttCmdTopic(),
			Timezone: d.Timezone,
		}
	}
	return json.Marshal(data)
}

type KafkaRemoteSend interface {
	Remote(device *RealtimeDevice, setValues cache.ValueCache, deviceNo, address uint8, val float64) error
	Realtime(*RealtimeDevice) error
	Close()
}

func NewKafkaRemote(ctx context.Context, di kafka.ConfigDI, l log.Logger) KafkaRemoteSend {
	topic := os.Getenv("KAFKA_TOPIC_REMOTE")
	kafka := kafka.NewCommonKafka(ctx, di, topic)
	kafka.SetLog(l)
	return &kafkaRemoteImpl{
		CommonKafka: kafka,
		topic:       topic,
	}
}

type kafkaRemoteImpl struct {
	kafka.CommonKafka
	topic string
}

func (s *kafkaRemoteImpl) Remote(device *RealtimeDevice, setValues cache.ValueCache, deviceNo, address uint8, val float64) error {
	if device.RawdataCache == nil {
		return errors.New("rawdata cache is empty")
	}
	if setValues == nil {
		return errors.New("data cache has no set values")
	}
	univalue := getSensorUniValue(deviceNo, address)
	controller := setValues.GetValue(univalue)
	if controller == nil {
		return fmt.Errorf("data cache has no controller device [%d] & address [%d]", deviceNo, address)
	}
	w := s.GetKafkaWriter()
	field := fmt.Sprintf("SET_%d_%d_%d", deviceNo, address, controller.Dp)
	var setVal float64
	if controller.Dp == 0 {
		setVal = val
	} else {
		setVal = val * math.Pow10(int(controller.Dp))
	}
	remote := &kafkaDao.PublishRemote{
		Topic: device.GetMqttCmdTopic(),
	}
	err := remote.AddCmd(field, setVal)
	if err != nil {
		return err
	}
	message, err := json.Marshal(remote)
	if err != nil {
		return err
	}

	err = w.Message(nil, message)
	if err != nil {
		return err
	}
	return nil
}

func (s *kafkaRemoteImpl) Realtime(device *RealtimeDevice) error {
	w := s.GetKafkaWriter()
	remote := &kafkaDao.PublishRemote{
		Topic: device.GetMqttCmdTopic(),
	}
	err := remote.AddCmd("realtime", 1)
	if err != nil {
		return err
	}
	message, err := json.Marshal(remote)
	if err != nil {
		return err
	}

	err = w.Message(nil, message)
	if err != nil {
		return err
	}
	return nil
}

func getSensorUniValue(device, address uint8) uint32 {
	return uint32(device)*256 + uint32(address)
}
