package device

import (
	"context"
	"strings"

	cacheDao "device/dao/cache"
	"device/dao/mon"
	"device/types"

	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/log"
	"github.com/94peter/sterna/model/cache"
	"github.com/94peter/sterna/model/mgom"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type RealtimeDeviceData struct {
	*RealtimeDevice
	SetCache cacheDao.ValueCache
	getCache cacheDao.ValueCache
}

type RealtimeDevice struct {
	*cacheDao.RawdataCache
	*mon.Device
}

type RealtimeInfo interface {
	AllAvaliableDevice() ([]*RealtimeDevice, error)
	FindByID(id primitive.ObjectID) (*RealtimeDeviceData, error)
	FindDevice(mac string, virtualId uint8) (*RealtimeDeviceData, error)
	FindSetCache(deviceID string) (cacheDao.ValueCache, error)
	FindGetCache(deviceID string) (cacheDao.ValueCache, error)
}

func NewRealtimeInfo(ctx context.Context, db *mongo.Database, clt db.RedisClient, l log.Logger) RealtimeInfo {
	return &realtimeImpl{
		dbmodel:  mgom.NewMgoModel(ctx, db, l),
		cacheClt: clt,
		cache:    cache.NewRedisCache(clt),
		log:      l,
	}
}

type realtimeImpl struct {
	dbmodel  mgom.MgoDBModel
	cacheClt db.RedisClient
	cache    cache.Cache
	log      log.Logger
}

func (impl *realtimeImpl) AllAvaliableDevice() ([]*RealtimeDevice, error) {
	list, err := impl.dbmodel.Find(&mon.Device{}, primitive.M{
		"state": types.Used,
	})
	if err != nil {
		return nil, err
	}
	devices := list.([]*mon.Device)

	keys, err := impl.cacheClt.Keys("*")
	if err != nil {
		return nil, err
	}
	var result []*RealtimeDevice
	if len(keys) == 0 {
		for _, d := range devices {
			result = append(result, &RealtimeDevice{
				Device: d,
			})
		}
		return result, nil
	}

	deviceMap := map[string]*mon.Device{}
	for _, d := range devices {
		deviceMap[d.ID.Hex()] = d
	}

	objs, err := impl.cache.GetObjs(keys, &cacheDao.RawdataCache{})
	if err != nil {
		return nil, err
	}

	var ok bool
	var device *mon.Device
	for _, obj := range objs {
		device, ok = deviceMap[obj.GetKey()]
		if !ok {
			continue
		}
		result = append(result, &RealtimeDevice{
			Device:       device,
			RawdataCache: obj.(*cacheDao.RawdataCache),
		})
		delete(deviceMap, obj.GetKey())
	}
	for _, d := range deviceMap {
		result = append(result, &RealtimeDevice{
			Device: d,
		})
	}
	return result, nil
}

func (impl *realtimeImpl) FindDevice(mac string, virtualId uint8) (*RealtimeDeviceData, error) {
	id, err := mon.NewDeviceID(mac, virtualId)
	if err != nil {
		return nil, err
	}
	return impl.FindByID(id)
}

func (impl *realtimeImpl) FindByID(id primitive.ObjectID) (*RealtimeDeviceData, error) {
	device := &mon.Device{ID: id}
	err := impl.dbmodel.FindByID(device)
	if err != nil {
		return nil, err
	}
	rawdataCache := &cacheDao.RawdataCache{}
	err = impl.cache.GetObj(id.Hex(), rawdataCache)
	if err != nil {
		if !strings.Contains(err.Error(), "nil") {
			return nil, err
		}
	}
	setCache, err := impl.FindSetCache(id.Hex())
	if err != nil {
		return nil, err
	}
	getCache, err := impl.FindGetCache(id.Hex())
	if err != nil {
		return nil, err
	}
	return &RealtimeDeviceData{
		RealtimeDevice: &RealtimeDevice{
			Device:       device,
			RawdataCache: rawdataCache,
		},
		SetCache: setCache,
		getCache: getCache,
	}, nil
}

func (impl *realtimeImpl) FindSetCache(deviceID string) (cacheDao.ValueCache, error) {
	obj := cacheDao.NewSetValuesCache(deviceID)
	err := impl.findValueCache(deviceID, obj)
	if err != nil {
		return nil, err
	}
	return obj, nil
}
func (impl *realtimeImpl) FindGetCache(deviceID string) (cacheDao.ValueCache, error) {
	obj := cacheDao.NewGetValuesCache(deviceID)
	err := impl.findValueCache(deviceID, obj)
	if err != nil {
		return nil, err
	}
	return obj, nil
}

func (impl *realtimeImpl) findValueCache(deviceID string, obj cacheDao.ValueCache) error {
	err := impl.cache.GetObjHash(obj.GetKey(), obj)
	if err != nil {
		if !strings.Contains(err.Error(), "nil") {
			return err
		}
	}
	return nil
}
