package rawdata

import (
	"context"
	"encoding/json"
	"errors"
	"os"
	"strings"
	"time"

	kafkaDao "device/dao/kafka"
	"device/dao/mon"

	"bitbucket.org/muulin/interlib/device/pb"

	"github.com/94peter/sterna/kafka"
	"github.com/94peter/sterna/log"
	"github.com/94peter/sterna/model/mgom"
	"go.mongodb.org/mongo-driver/mongo"
)

type Backup interface {
	Handler(*pb.UpdateRawdataRequest) error
	Close()
}

func NewBackup(ctx context.Context, di kafka.ConfigDI, db *mongo.Database, l log.Logger) Backup {
	return &backupImpl{
		dbmodel:     mgom.NewMgoModel(ctx, db, l),
		CommonKafka: kafka.NewCommonKafka(ctx, di, os.Getenv("KAFKA_TOPIC_BACKUP")),
		l:           l,
	}
}

type backupImpl struct {
	kafka.CommonKafka
	dbmodel mgom.MgoDBModel
	l       log.Logger
}

func (impl *backupImpl) Handler(rawdata *pb.UpdateRawdataRequest) error {
	if rawdata.Type == pb.DataType_Controller {
		return errors.New("invalid rawdata type")
	}
	id, err := mon.NewDeviceID(rawdata.Data.Mac, uint8(rawdata.Data.VirtualID))
	if err != nil {
		return err
	}
	device := &mon.Device{
		ID: id,
	}
	err = impl.dbmodel.FindByID(device)
	if err != nil {
		if strings.Contains(err.Error(), "no documents in result") {
			return errors.New("backup fail: device not found")
		}
		return err
	}
	if device.Channel == "" {
		return errors.New("backup fail: device not set channel")
	}
	w := impl.GetKafkaWriter()
	if w == nil {
		return errors.New("backup fail: kafka writer is nil")
	}
	message, err := getMessage(rawdata)
	if err != nil {
		return err
	}
	err = w.Message(map[string][]byte{
		"X-Channel": []byte(device.Channel),
	}, message)
	if err != nil {
		return err
	}
	return nil
}

func getMessage(rawdata *pb.UpdateRawdataRequest) ([]byte, error) {

	var data *kafkaDao.PublishDeviceData

	dataTime, err := time.Parse(time.RFC3339, rawdata.Data.Time)
	if err != nil {
		return nil, err
	}
	data = &kafkaDao.PublishDeviceData{
		MacAddress:  rawdata.Data.GwID,
		VirtualId:   int(rawdata.Data.VirtualID),
		Type:        rawdata.Type,
		LatestTime:  uint32(dataTime.Unix()),
		OnlineState: "yellow",
		Now:         uint32(dataTime.Add(time.Duration(60 - dataTime.Second())).Unix()),
	}

	for k, v := range rawdata.Data.Values {
		data.AddValue(k, v.Value)
	}

	return json.Marshal(data)
}
