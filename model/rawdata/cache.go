package rawdata

import (
	"fmt"
	"time"

	cacheDao "device/dao/cache"
	"device/dao/mon"
	"device/types"

	"bitbucket.org/muulin/interlib/device/pb"

	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/log"
	"github.com/94peter/sterna/model/cache"
	"github.com/94peter/sterna/util"
	"github.com/go-redis/redis/v8"
)

type Cache interface {
	UpsertGet(*pb.UpdateRawdataRequest) error
}

func NewCache(clt db.RedisClient, l log.Logger) Cache {
	return &cacheImpl{
		Cache: cache.NewRedisCache(clt),
		l:     l,
	}
}

type cacheImpl struct {
	cache.Cache
	l log.Logger
}

const (
	maxRawdataInterval = time.Minute * 3
	minRawdataInterval = maxRawdataInterval * -1
)

// 更新新資料
func (impl *cacheImpl) UpsertGet(rawdata *pb.UpdateRawdataRequest) error {
	id, err := mon.NewDeviceID(rawdata.Data.Mac, uint8(rawdata.Data.VirtualID))
	if err != nil {
		return err
	}
	obj := &cacheDao.RawdataCache{
		Key: id.Hex(),
	}
	err = impl.Cache.GetObj(obj.Key, obj)
	if err != nil && err != redis.Nil {
		return err
	}
	getData := cacheDao.NewGetValuesCache(obj.Key)
	defer func() {
		d := obj.DataInterval + maxRawdataInterval
		err = impl.Cache.SaveObj(obj, d)
		if err != nil {
			impl.l.Err("save rawdata fail: " + err.Error())
		}
		err = impl.Cache.SaveObjHash(getData, d)
		if err != nil {
			impl.l.Err("save obj hash fail: " + err.Error())
		}
	}()
	dataTime, err := time.Parse(time.RFC3339, rawdata.Data.Time)
	if err != nil {
		dataTime = time.Now()
	}

	obj.DataInterval = time.Duration(int64(rawdata.Data.SendDuration)) * time.Second
	if obj.RemoteCmd == "" {
		obj.RemoteCmd = getMQTTRemotTopic(rawdata.Data.Mac, rawdata.Data.GwID)
	}

	// 三分鐘內的資料才接受
	currentTime := time.Now()
	diff := dataTime.Sub(currentTime)
	if diff < minRawdataInterval {
		obj.ErrorMessage = types.ErrDataTimeIsTooLate.Error()
		return types.ErrDataTimeIsTooLate
	} else if diff > maxRawdataInterval {
		return types.ErrDataTimeIsNotCorrect
	}

	obj.OnlineState = "green"
	if !obj.LatestTime.IsZero() &&
		obj.LatestTime.Sub(dataTime) > 30 {
		obj.ErrorMessage = types.ErrDataTimeIsTooLate.Error()
		return types.ErrDataTimeIsTooLate
	}
	obj.LatestTime = dataTime
	for k, v := range rawdata.Data.Values {
		getData.SetValue(k, v.Value, uint8(v.Dp), dataTime)
	}
	return nil
}

func getMQTTRemotTopic(mac, gwid string) string {
	return util.StrAppend("cmd/", util.MD5(fmt.Sprintf("%s%s", mac, gwid)))
}
