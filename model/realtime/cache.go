package realtime

import (
	"fmt"
	"time"

	cacheDao "device/dao/cache"
	"device/dao/mon"
	"device/types"

	"bitbucket.org/muulin/interlib/device/pb"

	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/log"
	"github.com/94peter/sterna/model/cache"
	"github.com/94peter/sterna/util"
	"github.com/go-redis/redis/v8"
)

type Cache interface {
	UpsertSetToMap(*pb.UpdateRawdataRequest) error
}

func NewCache(clt db.RedisClient, l log.Logger) Cache {
	return &cacheImpl{
		Cache: cache.NewRedisCache(clt),
		l:     l,
	}
}

type cacheImpl struct {
	cache.Cache
	l log.Logger
}

const (
	rawdataInterval = time.Minute * 3
	setDataExpired  = time.Hour * 24
)

func (impl *cacheImpl) UpsertSetToMap(rawdata *pb.UpdateRawdataRequest) error {
	id, err := mon.NewDeviceID(rawdata.Data.Mac, uint8(rawdata.Data.VirtualID))
	if err != nil {
		return err
	}
	obj := &cacheDao.RawdataCache{
		Key: id.Hex(),
	}
	err = impl.Cache.GetObj(obj.Key, obj)
	if err != nil && err != redis.Nil {
		return err
	}
	setData := cacheDao.NewSetValuesCache(obj.Key)
	defer func() {
		err = impl.Cache.SaveObj(obj, time.Duration(int64(rawdata.Data.SendDuration))+obj.DataInterval)
		if err != nil {
			impl.l.Err("save rawdata fail: " + err.Error())
		}
		err = impl.Cache.SaveObjHash(setData, setDataExpired)
		if err != nil {
			impl.l.Err("save set data fail: " + err.Error())
		}
	}()

	obj.DataInterval = rawdataInterval

	if obj.RemoteCmd == "" {
		obj.RemoteCmd = getMQTTRemotTopic(rawdata.Data.Mac, rawdata.Data.GwID)
	}

	// 檢查時間是否正確
	dataTime, err := time.Parse(time.RFC3339, rawdata.Data.Time)
	if err != nil {
		dataTime = time.Now()
	}
	curTime := time.Now()
	diff := curTime.Sub(dataTime)
	if diff > rawdataInterval || diff < -1*rawdataInterval {
		obj.ErrorMessage = types.ErrDataTimeIsNotCorrect.Error()
		return types.ErrDataTimeIsNotCorrect
	}

	obj.LatestTime = dataTime
	for k, v := range rawdata.Data.Values {
		setData.SetValue(k, v.Value, uint8(v.Dp), dataTime)
	}

	return nil
}

func getMQTTRemotTopic(mac, gwid string) string {
	return util.StrAppend("cmd/", util.MD5(fmt.Sprintf("%s%s", mac, gwid)))
}
