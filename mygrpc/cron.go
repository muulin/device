package mygrpc

import (
	"context"
	"errors"
	"net/http"
	"sync"

	"device/model/device/v1"

	"bitbucket.org/muulin/interlib/device/pb"

	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/kafka"
	sternaLog "github.com/94peter/sterna/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

func NewCronService() pb.DeviceCronServiceServer {
	return &cronServer{}
}

type cronServer struct {
	pb.UnimplementedDeviceCronServiceServer
}

// PublishDeviceData publishes device data
func (s *cronServer) PublishDeviceData(ctx context.Context, empty *emptypb.Empty) (*pb.Response, error) {
	l := sternaLog.GetLogByCtx(ctx)

	dbclt := db.GetMgoDBClientByCtx(ctx)
	clt, err := getCache(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	kafkaDI, err := getKafkaDI(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	realtimeInfo := device.NewRealtimeInfo(ctx, dbclt.GetCoreDB(), clt, l)
	rd, err := realtimeInfo.AllAvaliableDevice()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	deviceSend := device.NewKafkaDeviceDataSend(context.Background(), kafkaDI)
	defer deviceSend.Close()

	var wg sync.WaitGroup
	for _, real := range rd {
		wg.Add(1)
		go func(r *device.RealtimeDevice, info device.RealtimeInfo) {
			defer wg.Done()
			getCache, err := info.FindGetCache(r.ID.Hex())
			if err != nil {
				l.Warn("find get chace fail: " + err.Error())
			}
			err = deviceSend.Send(r, getCache)
			if err != nil {
				l.Warn("kafka send fail: " + err.Error())
			}
		}(real, realtimeInfo)
	}
	wg.Wait()

	return &pb.Response{
		StatusCode: http.StatusOK,
		Message:    "",
	}, nil
}

func getCache(ctx context.Context) (db.RedisClient, error) {
	di := getServiceDI(ctx)
	if di == nil {
		return nil, errors.New("invalid di")
	}

	servName := di.GetServiceName()
	return di.NewRedisClientDB(ctx, di.GetDB(servName))
}

func getKafkaDI(ctx context.Context) (kafka.ConfigDI, error) {
	di := getServiceDI(ctx)
	if di == nil {
		return nil, errors.New("invalid di")
	}
	return di, nil
}
