package mygrpc

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"time"

	device "device/model/device/v1"
	"device/model/rawdata"
	"device/model/realtime"
	"device/types"

	"bitbucket.org/muulin/interlib/device/pb"

	"github.com/94peter/sterna"
	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/kafka"
	sternaLog "github.com/94peter/sterna/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type serviceDI interface {
	db.RedisDI
	kafka.ConfigDI
	sterna.CommonDI
}

func getServiceDI(ctx context.Context) serviceDI {
	val := sterna.GetDiByCtx(ctx)
	if di, ok := val.(serviceDI); ok {
		return di
	}
	return nil
}

func NewDeviceService() pb.DeviceServiceServer {
	return &deviceService{}
}

type deviceService struct {
	pb.UnimplementedDeviceServiceServer
}

// 更新裝置上傳數據
func (s *deviceService) UpdateRawdata(stream pb.DeviceService_UpdateRawdataServer) error {
	log := sternaLog.GetLogByCtx(stream.Context())
	di := getServiceDI(stream.Context())
	if di == nil {
		return status.Error(codes.Internal, "invalid di")
	}
	servName := di.GetServiceName()
	clt, err := di.NewRedisClientDB(stream.Context(), di.GetDB(servName))
	if err != nil {
		return status.Error(codes.Internal, err.Error())
	}
	crud := rawdata.NewCache(clt, log)
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			break
		} else if err != nil {
			sErr := stream.Send(&pb.Response{
				StatusCode: http.StatusBadRequest,
				Message:    err.Error(),
			})
			if sErr != nil {
				return sErr
			}
			continue
		}
		if req.Type == pb.DataType_Controller {
			stream.Send(&pb.Response{
				StatusCode: http.StatusBadRequest,
				Message:    "not get values",
			})
		}
		switch err = crud.UpsertGet(req); err {
		case types.ErrDataTimeIsTooLate:
			// 改備份資料 datatime is late
			stream.Send(&pb.Response{
				StatusCode: http.StatusBadRequest,
				Message:    err.Error(),
			})
		case nil:
			// upser ok
			stream.Send(&pb.Response{
				StatusCode: http.StatusOK,
			})
		default:
			sErr := stream.Send(&pb.Response{
				StatusCode: http.StatusBadRequest,
				Message:    err.Error(),
			})
			if sErr != nil {
				return sErr
			}
		}
	}
	return nil
}

func (s *deviceService) GetVritualId(ctx context.Context, req *pb.GetVirtualIdRequest) (*pb.GetVirtualIdResponse, error) {
	log := sternaLog.GetLogByCtx(ctx)
	dbclt := db.GetMgoDBClientByCtx(ctx)
	crud := device.NewCRUD(ctx, dbclt.GetCoreDB(), log)
	virtualId, err := crud.GetVirtualId(req)
	if err != nil {
		return nil, err
	}
	return &pb.GetVirtualIdResponse{VirtualID: uint32(virtualId)}, nil
}

func (s *deviceService) GetVritualIdStream(stream pb.DeviceService_GetVritualIdStreamServer) error {
	ctx := stream.Context()
	log := sternaLog.GetLogByCtx(ctx)
	dbclt := db.GetMgoDBClientByCtx(ctx)
	crud := device.NewCRUD(ctx, dbclt.GetCoreDB(), log)
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}
		vid, _ := crud.GetVirtualId(req)
		stream.Send(&pb.GetVirtualIdStreamResponse{
			StatusCode: http.StatusOK,
			VirtualID:  uint32(vid),
		})
	}
	return nil
}

func (s *deviceService) BackupRawdata(stream pb.DeviceService_BackupRawdataServer) error {
	log := sternaLog.GetLogByCtx(stream.Context())

	kafkaDI, err := getKafkaDI(stream.Context())
	if err != nil {
		return status.Error(codes.Internal, err.Error())
	}
	dbclt := db.GetMgoDBClientByCtx(stream.Context())
	crud := rawdata.NewBackup(context.Background(), kafkaDI, dbclt.GetCoreDB(), log)
	defer crud.Close()
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			break
		} else if err != nil {
			sErr := stream.Send(&pb.Response{
				StatusCode: http.StatusBadRequest,
				Message:    err.Error(),
			})
			if sErr != nil {
				return sErr
			}
			continue
		}
		switch err = crud.Handler(req); err {
		case nil:
			stream.Send(&pb.Response{
				StatusCode: http.StatusOK,
			})
		default:
			sErr := stream.Send(&pb.Response{
				StatusCode: http.StatusBadRequest,
				Message:    err.Error(),
			})
			if sErr != nil {
				return sErr
			}
		}
	}
	return nil
}

func (s *deviceService) UpdateRealtime(stream pb.DeviceService_UpdateRealtimeServer) error {
	log := sternaLog.GetLogByCtx(stream.Context())
	di := getServiceDI(stream.Context())
	if di == nil {
		return status.Error(codes.Internal, "invalid di")
	}
	servName := di.GetServiceName()
	clt, err := di.NewRedisClientDB(stream.Context(), di.GetDB(servName))
	if err != nil {
		return status.Error(codes.Internal, err.Error())
	}
	crud := realtime.NewCache(clt, log)

	kafkaDI, err := getKafkaDI(stream.Context())
	if err != nil {
		return status.Error(codes.Internal, err.Error())
	}
	dbclt := db.GetMgoDBClientByCtx(stream.Context())
	mykafka := realtime.NewKafka(context.Background(), kafkaDI, dbclt.GetCoreDB(), log)
	defer mykafka.Close()
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			break
		} else if err != nil {
			sErr := stream.Send(&pb.Response{
				StatusCode: http.StatusBadRequest,
				Message:    err.Error(),
			})
			if sErr != nil {
				return sErr
			}
			continue
		}
		if req.Type == pb.DataType_Sensor {
			stream.Send(&pb.Response{
				StatusCode: http.StatusBadRequest,
				Message:    "not set values",
			})
		}

		switch err = crud.UpsertSetToMap(req); err {
		case types.ErrDataTimeIsNotCorrect:
			// 改備份資料 datatime is late
			stream.Send(&pb.Response{
				StatusCode: http.StatusBadRequest,
				Message:    err.Error(),
			})
		case nil:
			// upser ok
			mykafka.Send(req)
			stream.Send(&pb.Response{
				StatusCode: http.StatusOK,
			})
		default:
			sErr := stream.Send(&pb.Response{
				StatusCode: http.StatusBadRequest,
				Message:    err.Error(),
			})
			if sErr != nil {
				return sErr
			}
		}
	}
	return nil
}

func (s *deviceService) SetTime(ctx context.Context, mydevice *pb.Device) (*pb.Response, error) {
	log := sternaLog.GetLogByCtx(ctx)
	dbclt := db.GetMgoDBClientByCtx(ctx)
	clt, err := getCache(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	info := device.NewRealtimeInfo(ctx, dbclt.GetCoreDB(), clt, log)
	deviceInfo, err := info.FindDevice(mydevice.Mac, uint8(mydevice.VirtualID))
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	if deviceInfo.RawdataCache == nil {
		return &pb.Response{
			StatusCode: http.StatusBadRequest,
			Message:    "cache not exist device: " + deviceInfo.ID.Hex(),
		}, nil
	}

	kafkaDI, err := getKafkaDI(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	mykafka := device.NewKafkaSetTime(context.Background(), kafkaDI)
	defer mykafka.Close()
	err = mykafka.Send(deviceInfo.RealtimeDevice)
	if err != nil {
		return nil, status.Error(codes.Internal, "send kafka fail: "+err.Error())
	}
	return &pb.Response{StatusCode: http.StatusOK}, nil
}

// 遠端控制
func (s *deviceService) Remote(ctx context.Context, req *pb.RemoteRequest) (*pb.RemoteResponse, error) {
	log := sternaLog.GetLogByCtx(ctx)
	dbclt := db.GetMgoDBClientByCtx(ctx)
	clt, err := getCache(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	kafkaDI, err := getKafkaDI(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	mykafka := device.NewKafkaRemote(context.Background(), kafkaDI, log)
	defer mykafka.Close()
	info := device.NewRealtimeInfo(ctx, dbclt.GetCoreDB(), clt, log)
	deviceInfo, err := info.FindDevice(req.Device.Mac, uint8(req.Device.VirtualID))
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	unitAddress := uint32(req.DeviceNo)*256 + uint32(req.Address)
	var oldVal float64
	if deviceInfo.RawdataCache == nil || deviceInfo.SetCache == nil || deviceInfo.SetCache.IsEmpty() {
		// no data cache or set values call realtime
		rawdataCacheExist := false
		err = mykafka.Realtime(deviceInfo.RealtimeDevice)
		if err != nil {
			return nil, status.Error(codes.Internal, "realtime fail: "+err.Error())
		}
		// check max 10 times
		const checkDuration = time.Second * 3
		for i := 0; i < 10; i++ {
			time.Sleep(checkDuration)
			deviceInfo, err = info.FindDevice(req.Device.Mac, uint8(req.Device.VirtualID))
			if err != nil {
				return nil, status.Error(codes.Internal, err.Error())
			}
			if deviceInfo.SetCache != nil && !deviceInfo.SetCache.IsEmpty() {
				if sensor := deviceInfo.SetCache.GetValue(unitAddress); sensor != nil {
					rawdataCacheExist = true
					oldVal = sensor.Value
					break
				}
			}
		}
		if !rawdataCacheExist {
			return &pb.RemoteResponse{
				StatusCode: http.StatusNotFound,
				Message:    "cache not exist device: " + deviceInfo.ID.Hex(),
			}, nil
		}
	} else {
		if sensor := deviceInfo.SetCache.GetValue(unitAddress); sensor != nil {
			oldVal = sensor.Value
		} else {
			return &pb.RemoteResponse{
				StatusCode: http.StatusNotFound,
				Message:    fmt.Sprintf("cache not exist set value: %d-%d", req.DeviceNo, req.Address),
			}, nil
		}
	}
	if oldVal == req.Value {
		return &pb.RemoteResponse{StatusCode: http.StatusBadRequest, Message: "remote value is not change"}, nil
	}

	err = mykafka.Remote(deviceInfo.RealtimeDevice, deviceInfo.SetCache, uint8(req.DeviceNo), uint8(req.Address), req.Value)
	if err != nil {
		return nil, status.Error(codes.Internal, "send kafka remote fail: "+err.Error())
	}
	return &pb.RemoteResponse{StatusCode: http.StatusOK, OriginValue: oldVal}, nil
}
