package mygrpc

import (
	"context"
	"errors"
	"io"
	"net/http"

	"device/dao/mon"
	device "device/model/device/v1"

	"bitbucket.org/muulin/interlib/device/pb"

	"github.com/94peter/sterna/db"
	sternaLog "github.com/94peter/sterna/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func NewServiceV1() pb.DeviceV1ServiceServer {
	return &ServerV1{}
}

type ServerV1 struct {
	pb.UnimplementedDeviceV1ServiceServer
}

// 新增裝置
func (s *ServerV1) CreateV1(stream pb.DeviceV1Service_CreateV1Server) error {
	log := sternaLog.GetLogByCtx(stream.Context())
	di := getServiceDI(stream.Context())
	if di == nil {
		return status.Error(codes.Internal, "invalid di")
	}
	clt := db.GetMgoDBClientByCtx(stream.Context())

	crud := device.NewCRUD(stream.Context(), clt.GetCoreDB(), log)
	var device *mon.Device
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			break
		} else if err != nil {
			sErr := stream.Send(&pb.CreateDeviceV1Response{
				StatusCode: http.StatusBadRequest,
				Message:    err.Error(),
			})
			if sErr != nil {
				return sErr
			}
			continue
		}
		switch device, err = crud.Create(req); err {

		case nil:
			stream.Send(&pb.CreateDeviceV1Response{
				StatusCode: http.StatusOK,
				Id:         device.ID.Hex(),
				VirtualID:  uint32(device.VirtualID),
			})
		default:
			sErr := stream.Send(&pb.CreateDeviceV1Response{
				StatusCode: http.StatusBadRequest,
				Message:    err.Error(),
			})
			if sErr != nil {
				return sErr
			}
		}
	}
	return nil
}

// 檢查裝置是否存在
func (s *ServerV1) CheckExist(ctx context.Context, req *pb.CheckExistRequest) (*pb.CheckExistResponse, error) {
	dbclt := db.GetMgoDBClientByCtx(ctx)
	log := sternaLog.GetLogByCtx(ctx)
	crud := device.NewCRUD(ctx, dbclt.GetCoreDB(), log)

	u, err := crud.DeviceExist(req)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return u, nil
}

func (s *ServerV1) Delete(stream pb.DeviceV1Service_DeleteServer) error {
	log := sternaLog.GetLogByCtx(stream.Context())
	di := getServiceDI(stream.Context())
	if di == nil {
		return errors.New("invalid di")
	}
	clt := db.GetMgoDBClientByCtx(stream.Context())

	crud := device.NewCRUD(stream.Context(), clt.GetCoreDB(), log)
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			break
		} else if err != nil {
			sErr := stream.Send(&pb.Response{
				StatusCode: http.StatusBadRequest,
				Message:    err.Error(),
			})
			if sErr != nil {
				return sErr
			}
			continue
		}
		switch err = crud.Remove(req); err {
		case nil:
			stream.Send(&pb.Response{
				StatusCode: http.StatusOK,
			})
		default:
			sErr := stream.Send(&pb.Response{
				StatusCode: http.StatusBadRequest,
				Message:    err.Error(),
			})
			if sErr != nil {
				return sErr
			}
		}
	}
	return nil
}

func (s *ServerV1) CheckState(ctx context.Context, req *pb.GetStateRequest) (*pb.GetStateResponse, error) {
	di := getServiceDI(ctx)
	if di == nil {
		return nil, status.Error(codes.Internal, "invalid di")
	}
	servName := di.GetServiceName()
	clt, err := di.NewRedisClientDB(ctx, di.GetDB(servName))
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	log := sternaLog.GetLogByCtx(ctx)

	deviceCache := device.NewCache(clt, log)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	stateMap := deviceCache.GetStateMap(req.Devices)
	result := map[string]string{}
	for k, v := range stateMap {
		result[k] = v.OnineState
	}
	return &pb.GetStateResponse{StateMap: result}, nil
}
