package mygrpc

import (
	"context"

	"device/dao/mon"
	"device/model/device/v1"

	"bitbucket.org/muulin/interlib/device/pb"

	"github.com/94peter/sterna/db"
	sternaLog "github.com/94peter/sterna/log"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func NewServiceV2() pb.DeviceV2ServiceServer {
	return &ServerV2{}
}

type ServerV2 struct {
	pb.UnimplementedDeviceV2ServiceServer
}

func (s *ServerV2) CheckState(ctx context.Context, req *pb.GetStateRequest) (*pb.GetStateV2Response, error) {
	di := getServiceDI(ctx)
	if di == nil {
		return nil, status.Error(codes.Internal, "invalid di")
	}
	servName := di.GetServiceName()
	clt, err := di.NewRedisClientDB(ctx, di.GetDB(servName))
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	log := sternaLog.GetLogByCtx(ctx)

	deviceCache := device.NewCache(clt, log)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	stateMap := deviceCache.GetStateMap(req.Devices)

	dbclt := db.GetMgoDBClientByCtx(ctx)

	crud := device.NewCRUD(ctx, dbclt.GetCoreDB(), log)
	ids := make([]primitive.ObjectID, len(req.Devices))
	for i, d := range req.Devices {
		id, err := mon.NewDeviceID(d.Mac, uint8(d.VirtualID))
		if err != nil {
			continue
		}
		ids[i] = id
	}
	devices, err := crud.FindByIds(ids)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	v2stateMap := map[string]*pb.DeviceState{}

	for _, device := range devices {
		state := stateMap[device.ID.Hex()]

		v2stateMap[device.ID.Hex()] = &pb.DeviceState{
			State:          string(device.State),
			OfflineNotify:  device.Notification.Offline,
			OnlineState:    state.OnineState,
			UploadDuration: int64(state.DataInterval),
		}
	}
	return &pb.GetStateV2Response{StateMap: v2stateMap}, nil
}
