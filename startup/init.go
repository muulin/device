package startup

import (
	"device/model/device/v1"
)

type StartUp interface {
	Run() error
}

func New(realInfo device.RealtimeInfo, remote device.KafkaRemoteSend) StartUp {
	return &startupImpl{
		info:   realInfo,
		remote: remote,
	}
}

type startupImpl struct {
	info   device.RealtimeInfo
	remote device.KafkaRemoteSend
}

func (impl *startupImpl) Run() error {
	// get all avaliable device
	devices, err := impl.info.AllAvaliableDevice()
	if err != nil {
		return err
	}
	for _, d := range devices {
		impl.remote.Realtime(d)
	}
	return nil
}
