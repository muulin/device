package types

import (
	"errors"
	"time"
)

type StartEndTime struct {
	StartStr 	string
	EndStr		string
	startTime 	time.Time
	endTime 	time.Time
}

// timeStr to time.Time,
// will make endtime to current time if endStr is "",
// if startStr == "", will change it to the 6months before the endTime
func (t *StartEndTime) Handle() error {
	var err error
	if t.EndStr == "" {
		t.endTime = time.Now()
	} else {
		t.endTime, err = time.Parse(time.RFC3339, t.EndStr)
		if err != nil {
			return errors.New("invalid endTime")
		}
	}
	if t.StartStr == "" {
		t.startTime = t.endTime.AddDate(0, -6, 0)
	} else {
		t.startTime, err = time.Parse(time.RFC3339, t.StartStr)
		if err != nil { 
			return errors.New("invalid startTime")
		}
	}
	if t.endTime.Unix() < t.startTime.Unix() {
		return errors.New("end time should not smaller than start time")
	}

	return nil
}

func (t *StartEndTime) GetTimeRange() (start, end time.Time, err error) {
	if t.startTime.IsZero() || t.endTime.IsZero() {
		return time.Time{}, time.Time{}, errors.New("StartEndTime need to Handle() 1st")
	}
	return t.startTime, t.endTime, nil
}