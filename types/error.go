package types

import "errors"

var (
	ErrDataTimeIsNotCorrect = errors.New("data time is not correct")
	ErrDataTimeIsTooLate    = errors.New("data time is too late")
)
