package types

import (
	"errors"
	"fmt"
	"strconv"
)

type PageDetail struct {
	LimitStr string
	PageStr  string
	limit  	int64
	page	int64
}

// if limit and page string is "",
// will default change limit to 20 and page to 1
func (p *PageDetail) Handle() error {
	var err error
	if p.PageStr == "" {
		p.PageStr = "1"
	}
	p.page, err = strconv.ParseInt(p.PageStr, 10, 64)
	if err != nil {
		return fmt.Errorf("%v\nstrconv problem (page)", err.Error())
	}
	if p.LimitStr == "" {
		p.LimitStr = "20"
	}
	p.limit, err = strconv.ParseInt(p.LimitStr, 10, 64)
	if err != nil {
		return fmt.Errorf("%v\nstrconv problem (limit)", err.Error())
	}

	return nil
}

func (p *PageDetail) GetDetail() (limit, page int64, err error) {
	if p.page == 0 || p.limit == 0 {
		return 0, 0, errors.New("page and limit should not be zero (call Handle() before this)")
	}
	return p.limit, p.page, nil
}