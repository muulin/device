package types

import (
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/bsontype"
)

type Timezone time.Time

const TimezoneFormat = "Z07:00"

func (ct Timezone) String() string {
	return time.Time(ct).Format(TimezoneFormat)
}

func (ct Timezone) Equal(tt Timezone) bool {
	return ct.String() == tt.String()
}

func (ct Timezone) MarshalYAML() (interface{}, error) {
	tt := time.Time(ct)
	return tt.Format(TimezoneFormat), nil
}

func (ct *Timezone) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var str string
	err := unmarshal(&str)
	if err != nil {
		return err
	}
	t, err := time.Parse(TimezoneFormat, str)
	if err != nil {
		return err
	}
	*ct = Timezone(t)
	return nil
}

func (ct *Timezone) UnmarshalJSON(b []byte) error {
	str := string(b)
	result := str[1 : len(str)-1]
	if len(result) == 0 {
		return nil
	}
	t, err := time.Parse(TimezoneFormat, result)
	if err != nil {
		return err
	}
	*ct = Timezone(t)
	return nil
}

func (ct Timezone) MarshalJSON() ([]byte, error) {
	tt := time.Time(ct)
	return []byte(fmt.Sprintf("\"%s\"", tt.Format(TimezoneFormat))), nil
}

func (ct Timezone) MarshalBSONValue() (bsontype.Type, []byte, error) {
	tt := time.Time(ct)
	return bson.MarshalValue(tt.Format(TimezoneFormat))
}

func (ct *Timezone) UnmarshalBSONValue(t bsontype.Type, b []byte) error {
	switch t {
	case bsontype.String:
		s := string(b[4 : len(b)-1])
		if s == "" {
			s = "+08:00"
		}
		t, err := time.Parse(TimezoneFormat, s)
		if err != nil {
			return err
		}
		*ct = Timezone(t)
	}
	return nil
}
