package types

import (
	"errors"

	"github.com/94peter/sterna/util"
)

type DeviceState string

const (
	// 入庫
	Stock   = DeviceState("stock")
	Sending = DeviceState("sending")
	// 已分配
	Assigned = DeviceState("assigned")
	// 使用中
	Used = DeviceState("used")
	// 待維修
	ToBeRepaired = DeviceState("2bRepaired")
	// 已回收
	Recycled = DeviceState("recycled")
	// 維修中
	Repairing = DeviceState("repairing")
	// 報廢
	Discard = DeviceState("discard")
)

func (ds DeviceState) Valid() error {
	if !util.IsStrInList(string(ds), string(Stock), string(Assigned),
		string(Sending), string(Used), string(ToBeRepaired),
		string(Recycled), string(Repairing), string(Discard)) {
		return errors.New("invalid action state")
	}

	return nil
}

type DeviceModel string

const (
	IoT627 = DeviceModel("IoT627")
	IoT909 = DeviceModel("IoT909")
	IoT512 = DeviceModel("IoT512")
	IoT524 = DeviceModel("IoT524")
	Scada  = DeviceModel("scada")
	Other  = DeviceModel("other")
)

func (dm DeviceModel) Valid() error {
	if !util.IsStrInList(string(dm), string(IoT627), string(IoT909), string(IoT512), string(IoT524), string(Scada)) {
		return errors.New("please check the input's model (400)")
	}
	return nil
}

type DeviceLogType string

const (
	DeviceStateChangedLog = DeviceLogType("stateChanged")
	DeviceUpdatedLog      = DeviceLogType("updated")
	// for set controller
	DeviceRemoteLog = DeviceLogType("remoteSet")
)

type TransType string

const (
	TransTypeSend    = TransType("sending")
	TransTypeRecycle = TransType("recycling")
)

func (t TransType) Valid() error {
	if !util.IsStrInList(string(t), string(TransTypeSend), string(TransTypeRecycle)) {
		return errors.New("invalid transType")
	}
	return nil
}

type TransState string

const (
	TransStateNew    = TransState("new")
	TransStateDone   = TransState("confirmed")
	TransStateCancel = TransState("canceled")
)

func (t TransState) Valid() error {
	if !util.IsStrInList(string(t), string(TransStateNew), string(TransStateDone), string(TransStateCancel)) {
		return errors.New("invalid transState")
	}
	return nil
}

type RepairedState string

const (
	RepairedStateNew         = RepairedState("new")
	RepairedStateReparable   = RepairedState("reparable")
	RepairedStateIrreparable = RepairedState("irreparable")
)

func (t RepairedState) Valid() error {
	if !util.IsStrInList(string(t), string(RepairedStateNew),
		string(RepairedStateReparable), string(RepairedStateIrreparable), "") {
		return errors.New("invalid repair state")
	}
	return nil
}

type DeviceValueType string

const (
	DeviceValueType_Get = DeviceValueType("get")
	DeviceValueType_Set = DeviceValueType("set")
)
