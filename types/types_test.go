package types_test

import (
	"encoding/json"
	"testing"
	"time"

	"device/types"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
)

func Test_TimezoneJson(t *testing.T) {
	type ss struct {
		T types.Timezone
	}
	timezone := types.Timezone(time.Now())
	b, err := json.Marshal(timezone)
	assert.Nil(t, err)
	newSS := &ss{}
	json.Unmarshal(b, newSS)
	assert.True(t, newSS.T.Equal(timezone))
}

func Test_TimezoneBson(t *testing.T) {
	type St struct {
		Str types.Timezone
	}
	timezone := St{Str: types.Timezone(time.Now())}
	b, err := bson.Marshal(timezone)
	assert.Nil(t, err)
	newTimezone := St{}

	err = bson.Unmarshal(b, &newTimezone)
	assert.Nil(t, err)
	assert.True(t, newTimezone.Str.Equal(timezone.Str))
}
